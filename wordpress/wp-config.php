<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'test');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'password');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '_E>MF0V]1o0iVRCVY*eZm9u)&pv<R]?>i8-cjv$n>+hg}Fy>#y^o<AS@6JhN6h.i');
define('SECURE_AUTH_KEY',  '0v~VwA]0o1-K<hm+G]^D68<Fg+Gb-*nNY/BC)-|p1F0h=eZy:.N,W ^qe%m!0uW/');
define('LOGGED_IN_KEY',    'O~rW4FLEih)HEOzyX,E{6Sou9.}r[]|.VfJ_xWnyw2I,{UC3dqhVy~_: V^v:hsz');
define('NONCE_KEY',        'qxVd;yq4rSCcir<JB#UB$axL>W.G|T=/p7=j4Fk)L@vMS^>2m+Hi!i_wa_Yx]Is(');
define('AUTH_SALT',        '^@U#B+2*_l9O6zEYX^-8h9+hZ0i.RuuNxybGVeKp`y}|[th]V5UN5r=2]tblNpI!');
define('SECURE_AUTH_SALT', '64k4#f,h)eht=6FGo#m`AxwxUp}EDyu4d0B?`Cpk^am#l5d%{12A.sWUqA;P5voN');
define('LOGGED_IN_SALT',   '{l?5TwmP4ug>!U?:zBu|qz*!e4=Ps$Cuw;OnjVn[Ujd,n5LI$<h>} 7$*0zx)[S7');
define('NONCE_SALT',       '*=x34Y^a|x.u*Z6cOuiG[92InY+r?i5sKNurC1ShRA]_)y%RdJ6UMs}A{UGE3T80');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
