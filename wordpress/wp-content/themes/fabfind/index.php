<?php
/**
 * The main template file
 *
 * @package WordPress
 * @subpackage FabFind
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<div class="bg-white">

    <div class="uk-container uk-container-small uk-padding">

        <div class="uk-width-small uk-align-center">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/icons/logo-dark.svg" class="uk-responsive-height logo-standard-height" alt="">
        </div><!-- ///uk-width-large -->

    </div><!-- //uk-container -->

</div><!-- //bg-white -->
<div class="uk-card uk-card-default uk-card-body uk-card-body-new uk-card-round">
</div>
<div class="bg-gray-light uk-padding-small">
        <div class="uk-container uk-container-small">

            <div class="max-wd-1600">
                <div class="uk-section">
                    <div class="uk-child-width-1@s" style="height: 30px;"></div>
                    <div class="uk-child-width-1@m uk-margin-small" uk-grid>

                        <p class="uk-text-center badges-wrap">
                            <span class="font-bolder font-gtWalsheimProMedium color-dark">Profile <span class="uk-badge badge-medium uk-hidden"></span></span>
                            <span class="font-bolder font-gtWalsheimProMedium color-dark">Saved <span class="uk-badge badge-medium bg-blue color-dark">0</span></span>
                            <span class="font-bolder font-gtWalsheimProMedium color-dark">Comparisons <span class="uk-badge badge-medium bg-dark-blue color-white">0</span></span>
                        </p>
                    </div>
                    <br><br>
                    <div class="uk-child-width-1@m">
                        <div class="uk-section uk-padding-remove list-item">
                            <div class="uk-container">

                                <?php
                                $args = array( 'post_type' => 'homes', 'posts_per_page' => 10 );
                                $the_query = new WP_Query( $args );
                                ?>
                                <?php if ( $the_query->have_posts() ) : ?>

                                <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

                                    <div class="uk-grid-match uk-child-width-1-3@m" uk-grid>
                                        <div class="uk-width-1-4@m list-left">
                                            <div class="uk-background-secondary bg-light" style="height: 230px;"></div>
                                        </div>
                                        <div class="uk-width-expand@m uk-padding-small list-content bg-white">
                                            <h4 class="font-large font-bolder uk-pad no-margin font-gtWalsheimProRegular"><?php the_title(); ?><span class="uk-align-right color-orange font-bolder font-gtWalsheimProRegular"><small>Method Homes</small></span></h4>
                                            <p class="no-margin font-gtWalsheimProRegular color-gray"><?php the_excerpt(); ?></p>
                                            <p class="uk-text-center list-meta">
                                                <span class="font-bolder font-gtPressuraMonoLight font-smaller color-dark">1200 - 2400 SQ FT</span>
                                                <span class="font-bolder font-gtPressuraMonoLight font-smaller color-dark">3 BED</span>
                                                <span class="font-bolder font-gtPressuraMonoLight font-smaller color-dark">2 BA</span>
                                                <span class="font-bolder font-gtPressuraMonoLight font-smaller color-dark">$125k - $412k</span>
                                                <span class="font-bolder font-gtPressuraMonoLight font-smaller color-dark">USA</span>
                                            </p>
                                        </div>
                                        <div class="uk-width-1-4@m list-right uk-padding left-border bg-white">
                                            <div class="font-bold font-gtPressuraMonoLight color-dark">Save<a class="uk-align-right color-dark font-bold" href="" uk-icon="icon: plus"></a></div>
                                            <div class="font-bold font-gtPressuraMonoLight color-dark">Compare<a class="uk-align-right color-dark font-bold" href="" uk-icon="icon: move"></a></div>
                                            <div class="font-bold font-gtPressuraMonoLight color-dark">View<a class="uk-align-right color-dark font-bold" href="<?php the_permalink(); ?>" uk-icon="icon: chevron-right"></a></div>
                                        </div>
                                    </div>
                                    <br>
                                <?php endwhile; ?>

                                <?php else : ?>

                                <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>

                                <?php endif; ?>

                            </div><!-- //uk-container -->
                        </div><!-- //uk-section -->
                    </div><!-- //uk-child-width -->

                </div><!-- //uk-section -->
            </div><!-- //max-wd-1600 -->

        </div><!-- //uk-container -->
    </div><!-- //bg-gray-light -->

<?php get_footer(); ?>
