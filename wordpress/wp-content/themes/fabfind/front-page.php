<?php
/**
 * The front page template file
 *
 * If the user has selected a static page for their homepage, this is what will
 * appear.
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage FabFind
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>
			<div class="uk-position-relative">
			    <div class="">
				    <img style="min-height: 200px;" width="100%" src="<?php header_image(); ?>" alt="">
				    <div class="uk-position-top">
				        <div class="uk-container uk-container-small">
					        <nav class="uk-navbar-container uk-navbar-transparent" uk-navbar>

					            <div class="uk-navbar-left uk-visible@s">
					                <a href="index.html"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/icons/logo-light.svg" class="uk-navbar-item uk-logo logo-standard-height" alt=""></a>
					            </div><!-- //uk-navbar-left visible -->
					            <div class="uk-navbar-center uk-hidden@s">
					                <a href="index.html"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/icons/logo-light.svg" class="uk-navbar-item uk-logo logo-standard-height" alt=""></a>
					            </div><!-- //uk-navbar-left hidden -->

					            <div class="uk-navbar-right uk-hidden@s">
					            	<ul class="uk-navbar-nav">
					            		<li><a href="index.html"><span class="uk-margin-small-right color-white" uk-icon="icon: sign-in; ratio: 1.5"></span></a></li>
					            	</ul>
					            </div><!-- //uk-navbar-right hidden -->

								<div class="uk-navbar-right uk-visible@s">
					            	<ul class="uk-navbar-nav">
					            		<li><a href="index.html"><span class="uk-margin-small-right color-white" uk-icon="icon: sign-in; ratio: 2" uk-toggle="target: #my-id"></span></a></li>
					            	</ul>
					            </div><!-- //uk-navbar-right visible -->
                                 <!-- This is the modal -->
									<div id="my-id" uk-modal>
									    <div class="uk-modal-dialog uk-modal-body">
									        <h2 class="uk-modal-title">Sign In</h2>

									            <form method="post" class="uk-form">

											          <label>Email</label>
											          <input type="email" name="email" placeholder="email" class="uk-input" >

											           <label>Password</label>
											           <input type="password" name="password" placeholder="password" class="uk-input">
											           <br><br>
											           <input type="submit" name="submit" value="submit" class="uk-button uk-button-primary"><!-- Submit input -->

                                                </form>
                                                <br>
									        <button class="uk-modal-close" type="button">Close</button>
									    </div>
									</div><!-- modal -->
					        </nav>
				        </div><!-- //uk-container -->
				        <div class="uk-h4 uk-text-center jumbotron font-gtWalsheimProRegular">
							<p class="color-white font-gtWalsheimProMedium font-bold"><?php echo get_theme_mod('site_tagline') ? get_theme_mod('site_tagline') : 'The fastest way to compare<br>and buy prefab homes.'; ?></p>
						</div><!-- //uk-h4 -->
				    </div>
				</div><!-- //max-wd-1600 -->
			</div><!-- //uk-position-relative -->


			<div class="uk-section-muted">

				<div class="uk-container uk-container-small">
					<form uk-grid class="search-bar-form uk-grid-small">
					    <div class="uk-width-expand@s uk-width-1-2@m uk-padding-small">
					        <div class="uk-inline display-block">
							    <span class="uk-form-icon color-dark" uk-icon="icon: search"></span>
							    <input class="uk-input no-border font-gtPressuraMonoLight color-dark font-small font-bold" type="text" placeholder="Find your fab">
							</div>
					    </div>
					    <div class="uk-width-1-4@s uk-padding-small">
					        <select class="uk-select no-border font-gtPressuraMonoLight color-dark font-small font-bold">
				                <option>Manufacturers</option>
				                <option>Distributers</option>
				            </select>
					    </div>
					    <div class="uk-width-auto@s uk-width-1-4@m uk-padding-remove-horizontal">
					        <button class="uk-button uk-button-default no-border font-gtPressuraMonoLight text-capitalize color-dark font-small font-bold bg-blue  uk-padding-small search-btn">Search</button>
					    </div>
					</form>
				</div><!-- //uk-container -->

				<div class="uk-container uk-container-small">
					<div class="uk-section">
					    <div class="uk-container icons-wrap">

					        <div class="uk-grid-match icon-grid uk-child-width-1-4@m uk-child-width-1-2@s uk-child-width-1-2" uk-grid>

					            <div>
					                <div class="uk-padding-small border-white">
						                <div>
										    <a href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/icons/arrow.svg" alt="Image" class="uk-align-center"></a>
										</div>
						                <div class="uk-text-center font-small color-dark font-bolder font-gtWalsheimProRegular">Trending</div>
						            </div><!-- //uk-padding-small -->
					            </div>

					            <div>
					                <div class="uk-padding-small border-white">
						                <div>
										    <a href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/icons/sun.svg" alt="Image" class="uk-align-center"></a>
										</div>
						                <div class="uk-text-center font-small color-dark font-bolder font-gtWalsheimProRegular">Newest</div>
						            </div><!-- //uk-padding-small -->
					            </div>
					            <div>
					                <div class="uk-padding-small border-white">
						                <div>
										    <a href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/icons/weight-scale.svg" alt="Image" class="uk-align-center"></a>
										</div>
						                <div class="uk-text-center font-small color-dark font-bolder font-gtWalsheimProRegular">Economical</div>
						            </div><!-- //uk-padding-small -->
					            </div>
					            <div>
					                <div class="uk-padding-small border-white">
						                <div>
										    <a href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/icons/cam.svg" alt="Image" class="uk-align-center"></a>
										</div>
						                <div class="uk-text-center font-small color-dark font-bolder font-gtWalsheimProRegular">Timeline</div>
						            </div><!-- //uk-paddng-small -->
					            </div>

					        </div><!-- ///uk-grid-match -->

					    </div><!-- //uk-container -->
					</div><!-- //uk-section -->

				</div><!-- //uk-container -->

				<div class="uk-container uk-container-small uk-padding">
				    <div class="uk-container">

				        <div class="uk-grid-match icon-grid uk-child-width-1-2@m" uk-grid>

				            <div>
				                <div class="uk-padding-small bg-light">
					                <div class="uk-width-large uk-padding-small">
									    <h5 class="color-orange font-bold font-gtWalsheimProRegular">Trending Featured Blog</h5>
									    <div class="uk-background-secondary uk-light" style="height: 300px;"></div>
									    <h2 class="color-white font-bold font-gtWalsheimProMedium">Conversion to Tesla solar tiles</h2>
									</div>
					            </div><!-- //uk-padding-small -->
				            </div>
				            <div>
				            	<div class="uk-grid-match icon-grid uk-child-width-1@m" uk-grid>

						            <div>
						                <div class="uk-padding-small bg-light">
							                <div class="uk-width-large uk-padding-small">
											    <h5 class="color-orange font-bold font-gtWalsheimProRegular">Featured Fab</h5>
											    <div class="uk-background-secondary uk-light" style="height: 100px;"></div>
											    <h4 class="color-white font-bold font-gtWalsheimProMedium">Bert's Box</h4>
											</div>
									   	</div><!-- //uk-padding-small -->
								    </div>
						            <div>
						                <div class="uk-padding-small bg-light">
							                <div class="uk-width-large uk-padding-small">
											    <h5 class="color-orange font-bold font-gtWalsheimProRegular">Featured Manufacturer</h5>
											    <div class="uk-background-secondary uk-light" style="height: 100px;"></div>
											    <h4 class="color-white font-bold font-gtWalsheimProMedium">Turkel Design</h4>
											</div>

							            </div><!-- //uk-padding-small -->
						            </div>

						        </div><!-- ///uk-grid-match -->
				            </div>
				        </div><!-- ///uk-grid-match -->

				    </div><!-- //uk-container -->

				</div><!-- //uk-container -->

			</div><!-- //uk-section-muted -->

			<div class="uk-clearfix"></div>

			<div class="bg-gray-light uk-padding-large uk-padding-remove-bottom">
			    <div class="uk-container uk-container-small">

			    	<div class="max-wd-1600">
				        <div class="uk-grid-match icon-grid uk-child-width-1-2@m" uk-grid>

				            <div>
				                <div class="uk-width-large uk-align-center">
									<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/lead-actor.png">
								</div>
				            </div>

				            <div>
				                <h2 class="font-bolder color-dark font-gtWalsheimProMedium uk-text-center uk-margin-remove-bottom uk-padding-remove-bottom">Share your dream<br> with our team.</h2>
				                <hr class="hr-short">
							   	<p class="font-bold color-dark uk-text-center font-gtWalsheimProRegular">We're prefab fanitics and local market experts – and we're here to help make building your prefab home stress-free.</p>
							   	<div class="uk-text-center uk-padding-bsmall">
							   		<button class="uk-button uk-button-default font-bold color-dark action-btn uk-button-large font-gtPressuraMonoLight" uk-toggle="target: #my-id" >Build Your Profile</button>
				            	</div>
				            </div>


				        </div><!-- ///uk-grid-match -->
				    </div><!-- //max-wd-1600 -->

			    </div><!-- //uk-container -->
			</div><!-- //bg-gray-light -->

<?php get_footer(); ?>
