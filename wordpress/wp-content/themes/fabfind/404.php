<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<div class="bg-white">

    <div class="uk-container uk-container-small uk-padding">

        <div class="uk-width-small uk-align-center">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/icons/logo-dark.svg" class="" alt="">
        </div><!-- ///uk-width-large -->

    </div><!-- //uk-container -->

</div><!-- //priamry-blue -->
<div class="uk-card uk-card-default uk-card-body uk-card-body-new uk-card-round bg-blue uk-card-with-icon"><span uk-icon="icon: close; ratio: 3;" class="uk-text-center color-dark uk-animation-scale-up"></span></div>

<div class="bg-gray-light uk-padding-large">
    <div class="uk-container uk-container-small">

        <div class="max-wd-1600">
            <div class="uk-section">
                <div class="uk-child-width-1@m" uk-grid>
                    <h2 class="color-dark font-bold font-gtWalsheimProMedium uk-text-center">Not Found</h2>
                </div>
                <div class="uk-align-center uk-clearfix">
                    <hr class="hr-short-new hr-center"/>
                </div>

                <div class="font-gtWalsheimProRegular font-bold color-dark uk-text-center">
                    <p>Page that you've finding doesn't exist. Try another link from footer.</p>
                </div>
            </div><!-- //uk-section -->
        </div><!-- //max-wd-1600 -->

    </div><!-- //uk-container -->
</div><!-- //bg-gray-light -->

<?php get_footer(); ?>
