<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage FabFind
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<div class="bg-gray">
    <div class="uk-container uk-container-small uk-padding">

        <div class="uk-width-small uk-align-left">
            <img src="assets/icons/logo-light.svg" class="uk-responsive-height" alt="">
        </div><!-- ///uk-width-large -->
        <div class="uk-width-small uk-align-right font-gtWalsheimProRegular">
            <span class="uk-badge badge-medium border-blue bg-transparent font-bold color-white">0</span>&nbsp;
            <span class="uk-badge badge-medium border-dark-blue bg-transparent font-bold color-white">0</span>
        </div><!-- ///uk-width-large -->

    </div><!-- //uk-container -->

</div><!-- //bg-gray -->

<div class="uk-container top-margin-fix-slider">
    <div class="bannercontainer uk-hidden@m">
        <div class="banner carousel">
            <ul>
                <!-- THE BOXSLIDE EFFECT EXAMPLES  WITH LINK ON THE MAIN SLIDE EXAMPLE -->

                <li data-transition="boxslide" data-slotamount="7">
                   <img src="assets/images/gray-img.png">
                </li>
                <li data-transition="boxslide" data-slotamount="7">
                   <img src="assets/images/gray-img.png">
                 </li>
                 <li data-transition="boxslide" data-slotamount="7">
                   <img src="assets/images/gray-img.png">
                 </li>
            </ul>
        </div>
    </div>
</div>

<div class="uk-section-muted">
    <div class="uk-container uk-container-small">
        <div uk-grid class="top-margin-fix">

            <div class="uk-width-1-2@s uk-visible@m">

                <div class="uk-section uk-padding-remove-top uk-padding-remove-bottom">

                    <div class="uk-child-width-1@m">
                        <div class="uk-section uk-padding-remove list-item">
                            <div class="uk-container">

                                <div class="uk-grid-match uk-child-width-1-3@m icons-wrap-position" uk-grid>
                                    <div class="uk-width-1@m list-left uk-inline uk-margin">
                                        <?php the_post_thumbnail(); ?>
                                    </div>
                                    <div class="icons-wrap-position-fix uk-visible@m">
                                        <ul class="uk-list">
                                            <li class="font-gtWalsheimProRegular"><a href="" class="uk-icon-button bg-white color-dark font-bold uk-box-shadow-small" uk-icon="icon: plus"></a></li>
                                            <li class="font-gtWalsheimProRegular"><a href="" class="uk-icon-button bg-white color-dark font-bold uk-box-shadow-small" uk-icon="icon: move"></a></li>
                                            <li class="font-gtWalsheimProRegular"><a href="" class="uk-icon-button bg-white color-dark font-bold uk-box-shadow-small" uk-icon="icon: mail"></a></li>
                                            <li class="font-gtWalsheimProRegular"><a href="" class="uk-icon-button bg-white color-dark font-bold uk-box-shadow-small" uk-icon="icon: facebook"></a></li>
                                            <li class="font-gtWalsheimProRegular"><a href="" class="uk-icon-button bg-white color-dark font-bold uk-box-shadow-small" uk-icon="icon: youtube"></a></li>
                                            <li class="font-gtWalsheimProRegular"><a href="" class="uk-icon-button bg-white color-dark font-bold uk-box-shadow-small" uk-icon="icon: twitter"></a></li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="uk-grid-match uk-child-width-1-3@m" uk-grid>
                                    <div class="uk-width-expand@m list-left">
                                        <div class="uk-background-secondary bg-gray uk-inline uk-margin" style="height: 140px;">
                                            <div class="uk-position-cover uk-overlay uk-overlay-default uk-flex uk-flex-center uk-flex-middle">	<a class="color-white font-bold" href="" uk-icon="icon: play; ratio: 2"></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="uk-width-1-3@m list-content">
                                        <div class="uk-background-secondary bg-light" style="height: 140px;"></div>
                                    </div>
                                </div>

                                <div class="uk-grid-match uk-child-width-1-3@m" uk-grid>
                                    <div class="uk-width-1-2@m list-left">
                                        <div class="uk-background-secondary bg-light" style="height: 140px;"></div>
                                    </div>
                                    <div class="uk-width-1-2@m list-content">
                                        <div class="uk-background-secondary bg-light" style="height: 140px;"></div>
                                    </div>
                                </div>

                                <div class="uk-grid-match uk-child-width-1-3@m" uk-grid>
                                    <div class="uk-width-1@m list-left">
                                        <div class="uk-background-secondary bg-light" style="height: 220px;"></div>
                                    </div>
                                </div>


                                <div class="uk-grid-match uk-child-width-1-3@m" uk-grid>
                                    <div class="uk-width-1@m list-left">
                                        <div class="uk-background-secondary bg-light" style="height: 220px;"></div>
                                    </div>
                                </div>

                            </div><!-- //uk-container -->
                        </div><!-- //uk-section -->
                    </div><!-- //uk-child-width -->


                </div><!-- //uk-section -->

            </div><!-- //uk-width -->

            <?php while ( have_posts() ) : the_post(); ?>
            <div class="uk-width-1-2@m uk-width-1@s uk-padding uk-padding-remove-top">
                <div class="uk-width-small uk-visible@m">
                    <img src="assets/images/logo-stamp.png" class="uk-responsive-height" alt="">
                </div><!-- ///uk-width-large -->
                <div style="height: 30px;" class="uk-hidden@m"></div>
                <div class="">
                    <h2 class="font-bolder color-dark font-gtWalsheimProRegular uk-padding-small uk-padding-remove-horizontal uk-margin uk-margin-top"><?php the_title(); ?></h2>
                        <hr class="hr-short">
                        <p class="color-gray  font-gtWalsheimProRegular color-gray"><?php the_content(); ?></p>

                </div><!-- //div -->
                <br>

                <div class="uk-visible@s uk-hidden@m">
                    <ul class="uk-list uk-text-center">
                        <li class="font-gtWalsheimProRegular uk-display-inline-block"><a href="" class="uk-icon-button bg-white color-dark font-bold uk-box-shadow-small uk-margin-small-right" uk-icon="icon: plus"></a></li>
                        <li class="font-gtWalsheimProRegular uk-display-inline-block"><a href="" class="uk-icon-button bg-white color-dark font-bold uk-box-shadow-small uk-margin-small-right" uk-icon="icon: move"></a></li>
                        <li class="font-gtWalsheimProRegular uk-display-inline-block"><a href="" class="uk-icon-button bg-white color-dark font-bold uk-box-shadow-small uk-margin-small-right" uk-icon="icon: mail"></a></li>
                        <li class="font-gtWalsheimProRegular uk-display-inline-block"><a href="" class="uk-icon-button bg-white color-dark font-bold uk-box-shadow-small uk-margin-small-right" uk-icon="icon: facebook"></a></li>
                        <li class="font-gtWalsheimProRegular uk-display-inline-block"><a href="" class="uk-icon-button bg-white color-dark font-bold uk-box-shadow-small uk-margin-small-right" uk-icon="icon: youtube"></a></li>
                        <li class="font-gtWalsheimProRegular uk-display-inline-block"><a href="" class="uk-icon-button bg-white color-dark font-bold uk-box-shadow-small uk-margin-small-right" uk-icon="icon: twitter"></a></li>
                    </ul>
                </div>
                <div>
                    <h4 class="font-bolder color-dark font-gtWalsheimProRegular">Specifications</h4>
                     <div>
                        <ul class="uk-list">
                            <li class="white-border-bottom font-bold font-gtWalsheimProRegular color-dark">List item 1<span class="uk-align-right text">Modernist</span></li>
                            <li class="white-border-bottom font-bold font-gtWalsheimProRegular color-dark">Structure Type<span class="uk-align-right text">Modular Prefab</span></li>
                            <li class="white-border-bottom font-bold font-gtWalsheimProRegular color-dark">Living Sq. ft<span class="uk-align-right text">1430 - 2100</span></li>
                            <li class="white-border-bottom font-bold font-gtWalsheimProRegular color-dark">Deck Sq. ft<span class="uk-align-right text">800</span></li>
                            <li class="white-border-bottom font-bold font-gtWalsheimProRegular color-dark">Both<span class="uk-align-right text">2</span></li>
                            <li class="white-border-bottom font-bold font-gtWalsheimProRegular color-dark">Beds<span class="uk-align-right text">4</span></li>
                            <li class="white-border-bottom font-bold font-gtWalsheimProRegular color-dark">Height<span class="uk-align-right text">16</span></li>
                            <li class="white-border-bottom font-bold font-gtWalsheimProRegular color-dark">Length<span class="uk-align-right text">43</span></li>
                            <li class="white-border-bottom font-bold font-gtWalsheimProRegular color-dark">Width<span cl\ass="uk-align-right text">28</span></li>
                            <li class="white-border-bottom font-bold font-gtWalsheimProRegular color-dark">Country of origin<span class="uk-align-right text">USA</span></li>
                            <li class="white-border-bottom font-bold font-gtWalsheimProRegular color-dark">Estimated Cost<span class="uk-align-right text">$220,000h</span></li>
                        </ul>
                        <div class="uk-margin">
                            <div class="uk-form-controls">
                                <select uk-accordion="collapsible: true" class="uk-select no-border font-medium font-bold font-gtWalsheimProRegular color-dark bg-transparent uk-text-left">
                                    <option class="uk-accordion-content">Additional Features</option>
                                </select>
                            </div>
                            <div class="uk-form-controls">
                                <select uk-accordion="collapsible: true" class="uk-select no-border font-medium font-bold font-gtWalsheimProRegular color-dark bg-transparent uk-text-left">
                                    <option class="uk-accordion-content">Costs</option>
                                </select>
                            </div>
                            <div class="uk-form-controls">
                                <select uk-accordion="collapsible: true" class="uk-select no-border font-medium font-bold font-gtWalsheimProRegular color-dark bg-transparent uk-text-left">
                                    <option class="uk-accordion-content">Awards</option>
                                </select>
                            </div>
                            <div class="uk-form-controls">
                                <select uk-accordion="collapsible: true" class="uk-select no-border font-medium font-bold font-gtWalsheimProRegular color-dark bg-transparent uk-text-left">
                                    <option class="uk-accordion-content">Certifications</option>
                                </select>
                            </div>
                        </div>

                        <p uk-margin>
                            <a href="enquiry.html" class="border-white uk-button font-gtWalsheimProRegular font-bold bg-white color-dark text-capitalize">Inquiry</a>
                            <a class="border-white uk-button font-gtWalsheimProRegular font-bold bg-transparent color-dark text-capitalize">Save <span uk-icon="icon: plus"></span></a>
                            <a class="border-white uk-button font-gtWalsheimProRegular font-bold bg-transparent color-dark text-capitalize">Compare <span uk-icon="icon: move"></span></a>
                        </p>
                        <p uk-margin>

                            <a href="enquiry.html" class="border-white uk-button font-gtWalsheimProRegular font-bold bg-white color-dark text-capitalize">Inquiry</a>
                            <a class="border-white uk-button font-gtWalsheimProRegular font-bold bg-blue color-dark text-capitalize">Forget <small><span uk-icon="icon: close"></span></small></a>
                            <a class="border-white uk-button font-gtWalsheimProRegular font-bold bg-dark-blue color-white text-capitalize">Remove <small><span uk-icon="icon: ban"></span></small></a>
                        </p>

                     </div>
                </div><!-- //uk-width-large -->

            </div>

            <?php endwhile; ?>
        </div>

    </div><!-- //uk-container -->
    <div class="uk-container uk-container-small list-item uk-padding-remove-top uk-padding">
        <div><hr class="hr-short"/></div>
    </div>
    <div class="uk-container uk-container-small list-item uk-padding">

        <div class="uk-grid-match uk-child-width-1-3@m" uk-grid>
            <div class="uk-width-1-4@s list-left">
                <div class="uk-background-secondary bg-light" style="height: 230px;"></div>
            </div>
            <div class="uk-width-expand@s uk-padding-small list-content bg-white">
                <h4 class="font-large font-bolder uk-pad no-margin font-gtWalsheimProRegular">Elemental Series<span class="uk-align-right color-orange font-bolder font-gtWalsheimProRegular"><small>Method Homes</small></span></h4>
                <p class="no-margin font-gtWalsheimProRegular color-gray">The Method Homes Elemental Series is a line of modern, efficient, and livable modular homes. Elemental features 6 different floorplans for you to customize. To streamline the design...</p>
                <p class="uk-text-center list-meta">
                    <span class="font-bolder font-gtPressuraMonoLight font-smaller color-dark">1200 - 2400 SQ FT</span>
                    <span class="font-bolder font-gtPressuraMonoLight font-smaller color-dark">3 BED</span>
                    <span class="font-bolder font-gtPressuraMonoLight font-smaller color-dark">2 BA</span>
                    <span class="font-bolder font-gtPressuraMonoLight font-smaller color-dark">$125k - $412k</span>
                    <span class="font-bolder font-gtPressuraMonoLight font-smaller color-dark">USA</span>
                </p>
            </div>
            <div class="uk-width-1-4@s list-right uk-padding left-border bg-white">
                <div class="font-bold font-gtPressuraMonoLight color-dark">Save<a class="uk-align-right color-dark font-bold" href="" uk-icon="icon: plus"></a></div>
                <div class="font-bold font-gtPressuraMonoLight color-dark">Compare<a class="uk-align-right color-dark font-bold" href="" uk-icon="icon: move"></a></div>
                <div class="font-bold font-gtPressuraMonoLight color-dark">View<a class="uk-align-right color-dark font-bold" href="" uk-icon="icon: chevron-right"></a></div>
            </div>
        </div>

    </div><!-- //uk-container -->
    <!-- temporary fix -->
        <div style="height: 40px;" class="bg-transparent"></div>
    <!-- //temporary fix -->
</div>

<?php get_footer(); ?>
