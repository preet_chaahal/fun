<?php
/**
 * The template for displaying the footer
 *
 * @package WordPress
 * @subpackage FabFind
 * @since 1.0
 * @version 1.0
 */
?><!-- //Footer starts -->
<div class="bg-blue">

    <div class="uk-container uk-container-small uk-padding">

        <div class="uk-grid-match icon-grid uk-child-width-1-2@m" uk-grid>

            <div class="uk-width-1-3@m">
                <p class="font-bolder color-dark padding-fix font-gtWalsheimProRegular">Be the first to know!  Join the Fastfab Insider</p>
            </div>
            <div class="uk-width-expand@m">
                <form class="subscribe-form">
                    <div class="uk-margin" uk-margin>
                        <input class="uk-input uk-form-width-medium no-border font-gtPressuraMonoLight color-dark font-small font-bold uk-form-large uk-padding-small" type="text" placeholder="email@example.com">
                        <button class="uk-button uk-button-default font-gtPressuraMonoLight color-dark font-small font-bold uk-form-large action-btn">Subscribe</button>
                    </div><!-- //uk-margin -->
                </form>
            </div>

        </div><!-- //uk-grid-match -->

    </div><!-- //uk-container -->

</div><!-- //priamry-blue -->

<div class="bg-dark">
    <div class="uk-container uk-container-small uk-padding">
        <div class="uk-grid icon-grid uk-child-width-1@m" uk-grid>

            <div class="uk-width-1-3@m">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/icons/logo-light.svg" class="uk-responsive-height logo-standard-height" alt="">
            </div><!-- ///uk-width-large -->

        </div><!-- //uk-grid-match -->
        <div class="uk-grid-match uk-margin-top-small icon-grid uk-child-width-1-3@m" uk-grid>

            <div class="uk-width-auto@m uk-padding-small">
                <p class="color-white font-small">&copy; <span class=" font-gtPressuraMonoLight">Copyright 2017 Panthera Group</span></p>
            </div>
            <div class="uk-width-expand@m footer-links uk-padding-small">
                <p class="font-gtPressuraMonoLight font-small">
                    <a href="#" class="color-blue uk-display-inline-block uk-link-muted">Blog</a>
                    <a href="#" class="color-blue uk-display-inline-block uk-link-muted">Contact</a>
                    <a href="#" class="color-blue uk-display-inline-block uk-link-muted">Privacy</a>
                    <a href="#" class="color-blue uk-display-inline-block uk-link-muted">FAQ</a>
                    <a href="#" class="color-blue uk-display-inline-block uk-link-muted">Term & Conditions</a>
                </p>
            </div>
            <div class="uk-width-auto@m footer-social-icons uk-padding-small">
                <p>
                    <a href="#" class="color-white uk-display-inline-block" uk-icon="icon: twitter"></a>
                    <a href="#" class="color-white uk-display-inline-block" uk-icon="icon: instagram"></a>
                    <a href="#" class="color-white uk-display-inline-block" uk-icon="icon: rss"></a>
                </p>
            </div>

        </div><!-- //uk-grid-match -->
    </div><!-- //uk-container -->
</div><!-- //bg-dark -->
<!-- //Footer ends -->
</div>

<?php wp_footer(); ?>

</body>
</html>
