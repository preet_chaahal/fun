<?php /* Template Name: Profile Page */ ?>

<?php get_header(); ?>

<div class="bg-white">

    <div class="uk-container uk-container-small uk-padding">

        <div class="uk-width-small uk-align-center">
            <img src="assets/icons/logo-dark.svg" class="uk-responsive-height logo-standard-height" alt="">
        </div><!-- ///uk-width-large -->

    </div><!-- //uk-container -->

</div><!-- //priamry-blue -->
<div class="uk-card uk-card-default uk-card-body uk-card-body-new uk-card-round">
</div>

<div class="bg-gray-light uk-padding-large">
    <div class="uk-container uk-container-small">

        <div class="max-wd-1600">
            <div class="uk-section uk-padding-top">
                <div class="uk-child-width-1@m" uk-grid>

                    <p class="uk-text-center badges-wrap">
                        <span class="font-bolder font-gtWalsheimProMedium color-dark">Profile <span class="uk-badge badge-medium uk-hidden"></span></span>
                        <span class="font-bolder font-gtWalsheimProMedium color-dark">Saved <span class="uk-badge badge-medium bg-blue color-dark">3</span></span>
                        <span class="font-bolder font-gtWalsheimProMedium color-dark">Comparisons <span class="uk-badge badge-medium bg-dark-blue color-white">4</span></span>
                    </p>
                </div>
                <div class="uk-child-width-1@m">
                    <form class="uk-form-horizontal uk-margin-large uk-padding-large">

                        <div class="uk-margin flat-form-elem white-border-bottom font-gtWalsheimProRegular">
                            <label class="uk-form-label font-medium font-bold color-dark" for="form-horizontal-text">Name</label>
                            <div class="uk-form-controls">
                                <input class="uk-input font-medium font-bold color-dark uk-text-right" id="form-horizontal-text" type="text" placeholder="Some text..." value="John McGinn">
                            </div>
                        </div>
                        <div class="uk-margin flat-form-elem white-border-bottom font-gtWalsheimProRegular">
                            <label class="uk-form-label font-medium font-bold color-dark" for="form-horizontal-text">Password</label>
                            <div class="uk-form-controls">
                                <input class="uk-input font-medium font-bold color-dark uk-text-right" id="form-horizontal-text" type="password" value="password" placeholder="">
                            </div>
                        </div>
                        <div class="uk-margin flat-form-elem white-border-bottom font-gtWalsheimProRegular">
                            <label class="uk-form-label font-medium font-bold color-dark" for="form-horizontal-text">Address</label>
                            <div class="uk-form-controls">
                                <input class="uk-input font-medium font-bold color-dark uk-text-right" id="form-horizontal-text" type="text" value="Mt Pleasant, SC" placeholder="Some text...">
                            </div>
                        </div>
                        <div class="uk-margin flat-form-elem white-border-bottom font-gtWalsheimProRegular">
                            <label class="uk-form-label font-medium font-bold color-dark" for="form-horizontal-text">Phone</label>
                            <div class="uk-form-controls">
                                <input class="uk-input font-medium font-bold color-dark uk-text-right" id="form-horizontal-text" type="text" value="843 826 2159" placeholder="Some text...">
                            </div>
                        </div>

                        <div class="uk-margin flat-form-elem dark-blue-border-bottom">
                            <label class="uk-form-label font-medium font-bold color-dark" for="form-horizontal-select">Account Type</label>
                            <div class="uk-form-controls">
                                <select class="uk-select no-border font-medium font-bold color-dark uk-text-right">
                                    <option>Manufacturers</option>
                                    <option>Distributers</option>
                                </select>
                            </div>
                        </div>
                        <div class="uk-margin flat-form-elem white-border-bottom font-gtWalsheimProRegular">
                            <label class="uk-form-label font-medium font-bold color-dark" for="form-horizontal-text">Land Owner</label>
                            <div class="uk-form-controls">
                                <input class="uk-input font-medium font-bold color-dark uk-text-right" id="form-horizontal-text" type="text" placeholder="Some text...">
                            </div>
                        </div>
                        <div class="uk-margin flat-form-elem white-border-bottom font-gtWalsheimProRegular">
                            <label class="uk-form-label font-medium font-bold color-dark" for="form-horizontal-text">Budget</label>
                            <div class="uk-form-controls">
                                <input class="uk-input font-medium font-bold color-dark uk-text-right" id="form-horizontal-text" type="text" placeholder="Some text...">
                            </div>
                        </div>
                        <div class="uk-margin flat-form-elem white-border-bottom font-gtWalsheimProRegular">
                            <label class="uk-form-label font-medium font-bold color-dark" for="form-horizontal-text">Time Until Build</label>
                            <div class="uk-form-controls">
                                <input class="uk-input font-medium font-bold color-dark uk-text-right" id="form-horizontal-text" type="text" placeholder="Some text...">
                            </div>
                        </div>
                        <div class="uk-margin flat-form-elem white-border-bottom font-gtWalsheimProRegular">
                            <label class="uk-form-label font-medium font-bold color-dark" for="form-horizontal-text">Customer ID</label>
                            <div class="uk-form-controls">
                                <input class="uk-input font-medium font-bold color-dark uk-text-right" id="form-horizontal-text" type="text" placeholder="Some text...">
                            </div>
                        </div>
                        <div class="uk-margin">
                            <button class="uk-button uk-button-large uk-align-center uk-margin-small-bottom bg-white font-small font-bold color-dark font-gtPressuraMonoLight">Update Profile</button>
                            <button class="uk-button uk-button-large uk-align-center uk-margin-small-bottom font-small font-bold color-dark font-gtPressuraMonoLight">Remove Account</button>
                        </div><br><br>
                        <div class="uk-margin">
                            <button class="uk-button uk-button-large uk-align-center uk-margin-small-bottom bg-dark-blue font-small font-bold color-white font-gtPressuraMonoLight">Update Profile</button>
                            <button class="uk-button uk-button-large uk-align-center uk-margin-small-bottom font-small font-bold color-dark font-gtPressuraMonoLight">Remove Account</button>
                        </div>


                    </form>
                </div>

            </div><!-- //uk-section -->
        </div><!-- //max-wd-1600 -->

    </div><!-- //uk-container -->
</div><!-- //bg-gray-light -->
<?php get_footer(); ?>
