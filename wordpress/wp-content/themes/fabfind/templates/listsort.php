<?php /* Template Name : List Sort Page */?>

<?php get_header(); ?>

<div class="uk-offcanvas-content">

    <div class="bg-gray">

        <div class="uk-container uk-container-small uk-padding">

            <div class="uk-width-small uk-align-left">
                <img src="assets/icons/logo-light.svg" class="logo-standard-height" alt="">
            </div><!-- ///uk-width-large -->
            <div class="uk-width-small uk-align-right font-gtWalsheimProRegular">
                <span class="uk-badge badge-medium border-blue bg-transparent font-bold color-white">3</span>&nbsp;
                <span class="uk-badge badge-medium border-dark-blue bg-transparent font-bold color-white">4</span>
            </div><!-- ///uk-width-large -->

        </div><!-- //uk-container -->

    </div><!-- //bg-white -->

    <div class="uk-section-muted">

        <div class="uk-container uk-container-small">
            <form uk-grid class="search-bar-form">
                <div class="uk-width-1-2@s uk-padding-small">
                    <div class="uk-inline display-block">
                        <span class="uk-form-icon color-dark" uk-icon="icon: search"></span>
                        <input class="uk-input no-border font-gtPressuraMonoLight color-dark font-small font-bold" type="text" placeholder="Find your fab">
                    </div>
                </div>
                <div class="uk-width-1-4@s uk-padding-small">
                    <select class="uk-select no-border font-gtPressuraMonoLight color-dark font-small font-bold">
                        <option>Manufacturers</option>
                        <option>Distributers</option>
                    </select>
                </div>
                <div class="uk-width-1-4@s uk-padding-remove-horizontal">
                    <button class="uk-button uk-button-default no-border font-gtPressuraMonoLight text-capitalize color-dark font-small font-bold bg-blue  uk-padding-small search-btn">Search</button>
                </div>
            </form>
        </div><!-- //uk-container -->
        <div class="uk-container uk-container-small uk-padding">
            <p class="uk-text-center font-gtPressuraMonoLight font-bold font-small color-dark">Filter & Search <span style="padding-left: 20px;">
                <a href="#offcanvas-nav" class="color-dark" uk-toggle uk-icon="icon: thumbnails"></a>
            </p>
        </div><!-- //uk-container -->
        <div class="uk-container uk-container-small list-view-wrap">
            <div class="uk-grid-match uk-child-width-1-3@m" uk-grid>
                <div class="uk-width-1-4@m list-left">
                    <div class="uk-background-secondary bg-light" style="height: 230px;"></div>
                </div>
                <div class="uk-width-expand@m uk-padding-small list-content bg-white">
                    <h4 class="font-large font-bolder uk-pad no-margin font-gtWalsheimProRegular">Elemental Series<span class="uk-align-right color-orange font-bolder font-gtWalsheimProRegular"><small class="color-dark-blue font-bold font-small"><span uk-icon="icon: bolt"></span> Featured Result</small></span></h4>
                    <p class="uk-margin-remove uk-padding-remove font-gtWalsheimProRegular">The Method Homes Elemental Series is a line of modern, efficient, and livable modular homes. Elemental features 6 different floorplans for you to customize. To streamline the design...</p>
                    <p class="uk-text-center list-meta uk-margin-remove uk-padding-remove">
                        <span class="font-bolder font-gtPressuraMonoLight font-smaller color-dark">12MO</span>
                        <span class="font-bolder font-gtPressuraMonoLight font-smaller color-dark">3 BED</span>
                        <span class="font-bolder font-gtPressuraMonoLight font-smaller color-dark">2 BA</span>
                        <span class="font-bolder font-gtPressuraMonoLight font-smaller color-dark">$125k - $412k</span>
                        <span class="font-bolder font-gtPressuraMonoLight font-smaller color-dark">USA</span>
                    </p>
                </div>
                <div class="uk-width-1-4@m list-right uk-padding-small left-border bg-white">
                    <div class="font-bold font-gtPressuraMonoLight color-dark">Save<span class="uk-align-right"><a class="uk-icon-button color-dark font-bold" href="" uk-icon="icon: plus" uk-toggle="target: #my-id"></a></span></div>
                    <div class="font-bold font-gtPressuraMonoLight color-dark">Compare<span class="uk-align-right"><a class="uk-icon-button color-dark font-bold" href="" uk-icon="icon: move" uk-toggle="target: #myId"></a></span></div>
                    <div class="font-bold font-gtPressuraMonoLight color-dark">View<span class="uk-align-right"><a class="uk-icon-button color-dark font-bold" href="" uk-icon="icon: chevron-right"></a></span></div>
                </div>
            </div>
            <br>
            <div class="uk-grid-match uk-child-width-1-3@m" uk-grid>
                <div class="uk-width-1-4@m list-left">
                    <div class="uk-background-secondary bg-light" style="height: 230px;"></div>
                </div>
                <div class="uk-width-expand@m uk-padding-small list-content bg-white">
                    <h4 class="font-large font-bolder uk-pad no-margin font-gtWalsheimProRegular">Elemental Series<span class="uk-align-right color-orange font-bolder font-gtWalsheimProRegular uk-margin-remove"><small class=" font-bold font-small"><span uk-icon="icon: bolt"></span> Method Name</small></span></h4>
                    <p class="uk-margin-remove uk-padding-remove font-gtWalsheimProRegular">The Method Homes Elemental Series is a line of modern, efficient, and livable modular homes. Elemental features 6 different floorplans for you to customize. To streamline the design...</p>
                    <p class="uk-text-center list-meta uk-margin-remove uk-padding-remove">
                        <span class="font-bolder font-gtPressuraMonoLight font-smaller color-dark">12MO</span>
                        <span class="font-bolder font-gtPressuraMonoLight font-smaller color-dark">3 BED</span>
                        <span class="font-bolder font-gtPressuraMonoLight font-smaller color-dark">2 BA</span>
                        <span class="font-bolder font-gtPressuraMonoLight font-smaller color-dark">$125k - $412k</span>
                        <span class="font-bolder font-gtPressuraMonoLight font-smaller color-dark">USA</span>
                    </p>
                </div>
                <div class="uk-width-1-4@m list-right uk-padding-small left-border bg-white">
                    <div class="font-bold font-gtPressuraMonoLight color-dark">Save<span class="uk-align-right"><a class="uk-icon-button color-dark font-bold" href="" uk-icon="icon: plus" uk-toggle="target: #my-id"></a></span></div>
                    <div class="font-bold font-gtPressuraMonoLight color-dark">Compare<span class="uk-align-right"><a class="uk-icon-button color-dark font-bold" href="" uk-icon="icon: move" uk-toggle="target: #myId"></a></span></div>
                    <div class="font-bold font-gtPressuraMonoLight color-dark">View<span class="uk-align-right"><a class="uk-icon-button color-dark font-bold" href="" uk-icon="icon: chevron-right"></a></span></div>
                </div>
            </div>
            <br>
            <div class="uk-grid-match uk-child-width-1-3@m" uk-grid>
                <div class="uk-width-1-4@m list-left">
                    <div class="uk-background-secondary bg-light" style="height: 230px;"></div>
                </div>
                <div class="uk-width-expand@m uk-padding-small list-content bg-white">
                    <h4 class="font-large font-bolder uk-pad no-margin font-gtWalsheimProRegular">Elemental Series<span class="uk-align-right color-orange font-bolder font-gtWalsheimProRegular uk-margin-remove"><small class="font-bold font-small"><span uk-icon="icon: bolt"></span> Method Name</small></span></h4>
                    <p class="uk-margin-remove uk-padding-remove font-gtWalsheimProRegular">The Method Homes Elemental Series is a line of modern, efficient, and livable modular homes. Elemental features 6 different floorplans for you to customize. To streamline the design...</p>
                    <p class="uk-text-center list-meta uk-margin-remove uk-padding-remove">
                        <span class="font-bolder font-gtPressuraMonoLight font-smaller color-dark">12MO</span>
                        <span class="font-bolder font-gtPressuraMonoLight font-smaller color-dark">3 BED</span>
                        <span class="font-bolder font-gtPressuraMonoLight font-smaller color-dark">2 BA</span>
                        <span class="font-bolder font-gtPressuraMonoLight font-smaller color-dark">$125k - $412k</span>
                        <span class="font-bolder font-gtPressuraMonoLight font-smaller color-dark">USA</span>
                    </p>
                </div>
                <div class="uk-width-1-4@m list-right uk-padding-small left-border bg-white">
                    <div class="font-bold font-gtPressuraMonoLight color-dark">Save<span class="uk-align-right"><a class="uk-icon-button color-dark font-bold" href="" uk-icon="icon: plus" uk-toggle="target: #my-id"></a></span></div>
                    <div class="font-bold font-gtPressuraMonoLight color-dark">Compare<span class="uk-align-right"><a class="uk-icon-button color-dark font-bold" href="" uk-icon="icon: move" uk-toggle="target: #myId"></a></span></div>
                    <div class="font-bold font-gtPressuraMonoLight color-dark">View<span class="uk-align-right"><a class="uk-icon-button color-dark font-bold" href="" uk-icon="icon: chevron-right"></a></span></div>
                </div>
            </div>
            <br>

        </div><!-- //uk-container -->


    </div><!-- //uk-section-muted -->

    <!-- //Footer starts -->
    <div class="bg-blue">

        <div class="uk-container uk-container-small uk-padding">

            <div class="uk-grid-match icon-grid uk-child-width-1-2@m" uk-grid>

                <div class="uk-width-1-3@m">
                    <p class="font-bolder color-dark padding-fix font-gtWalsheimProRegular">Be the first to know!  Join the Fastfab Insider</p>
                </div>
                <div class="uk-width-expand@m">
                    <form class="subscribe-form">
                        <div class="uk-margin" uk-margin>
                            <input class="uk-input uk-form-width-medium no-border font-gtPressuraMonoLight color-dark font-small font-bold uk-form-large uk-padding-small" type="text" placeholder="email@example.com">
                            <button class="uk-button uk-button-default font-gtPressuraMonoLight color-dark font-small font-bold uk-form-large action-btn">Subscribe</button>
                        </div><!-- //uk-margin -->
                    </form>
                </div>

            </div><!-- //uk-grid-match -->

        </div><!-- //uk-container -->

    </div><!-- //priamry-blue -->

    <div class="bg-dark">
        <div class="uk-container uk-container-small uk-padding">
            <div class="uk-grid icon-grid uk-child-width-1@m" uk-grid>

                <div class="uk-width-1-3@m">
                    <img src="assets/icons/logo-light.svg" class="uk-responsive-height logo-standard-height" alt="">
                </div><!-- ///uk-width-large -->

            </div><!-- //uk-grid-match -->
            <div class="uk-grid-match uk-margin-top-small icon-grid uk-child-width-1-3@m" uk-grid>

                <div class="uk-width-auto@m uk-padding-small">
                    <p class="color-white font-small">&copy; <span class=" font-gtPressuraMonoLight">Copyright 2017 Panthera Group</span></p>
                </div>
                <div class="uk-width-expand@m footer-links uk-padding-small">
                    <p class="font-gtPressuraMonoLight font-small">
                        <a href="#" class="color-blue uk-display-inline-block uk-link-muted">Blog</a>
                        <a href="#" class="color-blue uk-display-inline-block uk-link-muted">Contact</a>
                        <a href="#" class="color-blue uk-display-inline-block uk-link-muted">Privacy</a>
                        <a href="#" class="color-blue uk-display-inline-block uk-link-muted">FAQ</a>
                        <a href="#" class="color-blue uk-display-inline-block uk-link-muted">Term & Conditions</a>
                    </p>
                </div>
                <div class="uk-width-auto@m footer-social-icons uk-padding-small">
                    <p>
                        <a href="#" class="color-white uk-display-inline-block" uk-icon="icon: twitter"></a>
                        <a href="#" class="color-white uk-display-inline-block" uk-icon="icon: instagram"></a>
                        <a href="#" class="color-white uk-display-inline-block" uk-icon="icon: rss"></a>
                    </p>
                </div>

            </div><!-- //uk-grid-match -->
        </div><!-- //uk-container -->
    </div><!-- //bg-dark -->
    <!-- //Footer ends -->



    <!-- //offcanvas -->

    <div id="offcanvas-nav" uk-offcanvas data-a="overlay: true">
        <div class="uk-offcanvas-bar uk-offcanvas-bar-light">

            <ul class="uk-nav uk-nav-default">
                <li class="uk-active uk-margin-medium"><a href="#"><img src="assets/images/logo-dark.png" class="uk-responsive-height" alt=""></a></li>
            </ul>
            <form>
                <fieldset class="uk-fieldset uk-margin-medium">
                    <legend class="uk-legend font-bolder font-small font-gtWalsheimProRegular color-dark uk-padding-remove-bottom uk-margin-remove-bottom">Sort Results</legend>
                    <div class="uk-margin uk-margin-remove">
                        <select class="uk-select no-border font-gtPressuraMonoLight color-dark font-smaller font-bold">
                            <option>By Popularity</option>
                            <option>By Relevance</option>
                        </select>
                    </div>
                </fieldset>
                <fieldset class="uk-fieldset uk-margin-medium">
                    <legend class="uk-legend font-bolder font-small font-gtWalsheimProRegular color-dark uk-padding-remove-bottom uk-margin-remove-bottom">Service Area</legend>
                    <div class="uk-margin uk-margin-remove">
                        <label class="font-gtPressuraMonoLight color-dark font-smaller font-bold"><span class="color-dark-blue font-bold">50 Miles</span> &nbsp;&nbsp;&nbsp;&nbsp;<span class="color-dark text-capitalize font-bold">from</span></label>
                        <input class="uk-input color-dark-blue font-gtPressuraMonoLight color-dark font-smaller font-bold" type="text" placeholder="Input" value="Mount Pleasant, SC">
                    </div>
                </fieldset>
                <fieldset class="uk-fieldset uk-margin-medium">
                    <legend class="uk-legend font-bolder font-small font-gtWalsheimProRegular color-dark uk-padding-remove-bottom uk-margin-remove-bottom">Listing Includes</legend>
                    <div class="uk-margin uk-margin-remove">
                        <label class="font-gtPressuraMonoLight color-dark font-smaller font-bold"><input class="uk-checkbox color-dark-blue font-gtPressuraMonoLight font-smaller font-bold" type="checkbox" checked> Photo</label>
                    </div><!-- //uk-margin -->
                    <div class="uk-margin uk-margin-remove">
                        <label class="color-dark font-gtPressuraMonoLight font-smaller font-bold"><input class="uk-checkbox color-dark-blue font-gtPressuraMonoLight color-dark font-smaller font-bold" type="checkbox"> Video</label>
                    </div><!-- //uk-margin -->
                </fieldset>
                <fieldset class="uk-fieldset uk-margin-medium">
                    <legend class="uk-legend font-bolder font-small font-gtWalsheimProRegular color-dark uk-padding-remove-bottom uk-margin-remove-bottom">Prefab Types</legend>
                    <div class="uk-margin uk-margin-remove">
                        <label class="color-dark font-gtPressuraMonoLight font-smaller font-bold"><input class="uk-checkbox color-dark-blue font-gtPressuraMonoLight font-smaller font-bold" type="checkbox" checked> Manufactured</label>
                    </div>
                    <div class="uk-margin uk-margin-remove">
                        <label class="color-dark font-gtPressuraMonoLight font-smaller font-bold"><input class="uk-checkbox color-dark-blue font-gtPressuraMonoLight font-smaller font-bold" type="checkbox"> Modular</label>
                    </div>
                    <div class="uk-margin uk-margin-remove">
                        <label class="color-dark font-gtPressuraMonoLight font-smaller font-bold"><input class="uk-checkbox color-dark-blue font-gtPressuraMonoLight font-smaller font-bold" type="checkbox"> Panel</label>
                    </div>
                    <div class="uk-margin uk-margin-remove">
                        <label class="color-dark font-gtPressuraMonoLight font-smaller font-bold"><input class="uk-checkbox color-dark-blue font-gtPressuraMonoLight font-smaller font-bold" type="checkbox"> Container</label>
                    </div>
                    <div class="uk-margin uk-margin-remove">
                        <label class="color-dark font-gtPressuraMonoLight font-smaller font-bold"><input class="uk-checkbox color-dark-blue font-gtPressuraMonoLight font-smaller font-bold" type="checkbox"> Movable</label>
                    </div>
                    <div class="uk-margin uk-margin-remove">
                        <label class="color-dark font-gtPressuraMonoLight font-smaller font-bold"><input class="uk-checkbox color-dark-blue font-gtPressuraMonoLight font-smaller font-bold" type="checkbox"> Custom</label>
                    </div>
                </fieldset>
                <fieldset class="uk-fieldset uk-margin-medium">
                    <legend class="uk-legend font-bolder font-small font-gtWalsheimProRegular color-dark uk-padding-remove-bottom uk-margin-remove-bottom">Building Style</legend>
                    <div class="uk-margin uk-margin-remove">
                        <label class="color-dark font-gtPressuraMonoLight font-smaller font-bold"><input class="uk-checkbox color-dark-blue font-gtPressuraMonoLight font-smaller font-bold" type="checkbox" checked> Modernist</label>
                    </div>
                    <div class="uk-margin uk-margin-remove">
                        <label class="color-dark font-gtPressuraMonoLight font-smaller font-bold"><input class="uk-checkbox color-dark-blue font-gtPressuraMonoLight font-smaller font-bold" type="checkbox"> Traditional</label>
                    </div>
                </fieldset>
                <fieldset class="uk-fieldset uk-margin-medium">
                    <legend class="uk-legend font-bolder font-small font-gtWalsheimProRegular color-dark uk-padding-remove-bottom uk-margin-remove-bottom">Square Feet</legend>
                    <div class="uk-margin uk-margin-remove uk-clearfix">
                        <label class="font-gtPressuraMonoLight color-dark font-smaller font-bold"><span class="uk-align-left font-gtPressuraMonoLight color-dark font-smaller font-bold">Any</span><span class="uk-align-right font-gtPressuraMonoLight color-dark font-smaller font-bold">5000+</span></label>
                    </div>
                    <div class="uk-margin uk-margin-remove uk-padding-remove uk-padding-remove-top">
                        <div id="slider"></div>
                    </div>
                </fieldset>
                <fieldset class="uk-fieldset uk-margin-medium">
                    <legend class="uk-legend font-bolder font-small font-gtWalsheimProRegular color-dark uk-padding-remove-bottom uk-margin-remove-bottom">Cost Per Foot</legend>
                    <div class="uk-margin uk-clearfix uk-margin-remove">
                        <label class="color-dark font-gtPressuraMonoLight font-smaller font-bold uk-text-center">
                            <input class="wd-35 uk-input uk-align-left color-dark uk-margin-remove light-gray-border-bottom font-gtPressuraMonoLight font-smaller font-bold" type="text" placeholder="0" value="Any">
                            <input class="wd-30 uk-input uk-form-width-small uk-align-left uk-text-center font-gtPressuraMonoLight color-dark font-smaller font-bold uk-margin-remove" type="text" placeholder="Small" value="to">
                            <input class="wd-35 uk-input uk-form-width-xsmall uk-align-right uk-text-right font-gtPressuraMonoLight color-dark font-smaller font-bold uk-margin-remove light-gray-border-bottom" type="text" placeholder="Small" value="$500+"></label>
                    </div>
                </fieldset>
                <fieldset class="uk-fieldset uk-margin-medium">
                    <legend class="uk-legend font-bolder font-small font-gtWalsheimProRegular color-dark uk-padding-remove-bottom uk-margin-remove-bottom">Bedrooms</legend>
                    <div class="uk-margin uk-clearfix uk-margin-remove">
                        <label class="color-dark font-gtPressuraMonoLight font-smaller font-bold">
                            <input class="wd-35 uk-input uk-align-left font-gtPressuraMonoLight color-dark font-smaller font-bold uk-margin-remove light-gray-border-bottom" type="text" placeholder="0" value="Any">
                            <input class="wd-30 uk-input uk-form-width-small uk-align-left uk-text-center font-gtPressuraMonoLight color-dark font-smaller font-bold uk-margin-remove" type="text" placeholder="Small" value="to">
                            <input class="wd-35 uk-input uk-form-width-xsmall uk-align-right uk-text-right font-gtPressuraMonoLight color-dark font-smaller font-bold uk-margin-remove light-gray-border-bottom" type="text" placeholder="Small" value="$500+"></label>
                    </div>
                </fieldset>
                <fieldset class="uk-fieldset uk-margin-medium">
                    <legend class="uk-legend font-bolder font-small font-gtWalsheimProRegular color-dark uk-padding-remove-bottom uk-margin-remove-bottom">Bathrooms</legend>
                    <div class="uk-margin uk-clearfix uk-margin-remove">
                        <label class="color-dark font-gtPressuraMonoLight font-smaller font-bold uk-text-center">
                            <input class="wd-35 uk-input uk-align-left font-gtPressuraMonoLight color-dark font-smaller font-bold uk-margin-remove light-gray-border-bottom" type="text" placeholder="0" value="Any">
                            <input class="wd-30 uk-input uk-form-width-small uk-align-left uk-text-center font-gtPressuraMonoLight color-dark font-smaller font-bold uk-margin-remove" type="text" placeholder="Small" value="to">
                            <input class="wd-35 uk-input uk-form-width-xsmall uk-align-right uk-text-right font-gtPressuraMonoLight color-dark font-smaller font-bold uk-margin-remove light-gray-border-bottom" type="text" placeholder="Small" value="$500+"></label>
                    </div>
                </fieldset>
                <fieldset class="uk-fieldset uk-margin-medium">
                    <legend class="uk-legend font-bolder font-small font-gtWalsheimProRegular color-dark uk-padding-remove-bottom uk-margin-remove-bottom">Keywords</legend>
                    <div class="uk-margin uk-margin-remove">
                    <input type="text" placeholder="Typing" class="uk-input font-gtPressuraMonoLight color-dark font-smaller font-bold light-gray-border-bottom">
                    </div>
                </fieldset>
                <fieldset class="uk-fieldset uk-margin-medium">
                        <legend class="uk-legend font-bolder font-small font-gtWalsheimProRegular color-dark uk-padding-remove-bottom uk-margin-remove-bottom">Additional Options <span class="uk-align-right font-gtPressuraMonoLight color-dark-blue font-smaller font-bold" uk-icon="icon: plus"></span></legend>
                </fieldset>
                <fieldset class="uk-fieldset uk-margin-medium">
                    <button class="uk-button no-border uk-margin-small font-gtPressuraMonoLight text-capitalize color-white font-small font-bold bg-dark-blue  uk-padding-xsmall search-btn">Apply</button>
                    <button class="uk-button uk-margin-xsmall uk-button-default no-border font-gtPressuraMonoLight text-capitalize color-dark-blue font-small font-bold bg-white  uk-padding-xsmall search-btn">Clear All</button>
                </fieldset>


            </form>

            <button class="uk-button uk-button-default uk-offcanvas-close uk-width-1-1 uk-margin" type="button">Close</button>

        </div>
    </div>
    <!-- //ofcanvas -->

</div>

<!-- This is the modal -->
<div id="my-id" uk-modal>
   <div class="uk-modal-dialog uk-modal-body">
       <h2 class="uk-modal-title">Saved Houses</h2>
         <img src="assets/images/house-4.png">

           <br>
       <button class="uk-modal-close" type="button">Close</button>
   </div>
</div><!-- modal -->

<!-- This is the modal -->
<div id="myId" uk-modal>
   <div class="uk-modal-dialog uk-modal-body">
       <h2 class="uk-modal-title">Compare Houses</h2>
         <img src="assets/images/house-4.png">
         <img src="assets/images/house-3.png">
           <br>
       <button class="uk-modal-close" type="button">Close</button>
   </div>
</div><!-- modal -->

<?php get_footer(); ?>
