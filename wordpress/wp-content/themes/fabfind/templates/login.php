<?php /* Template Name : Login Page*/ ?>

<?php get_header(); ?>

<div class="bg-white">

    <div class="uk-container uk-container-small uk-padding">

        <div class="uk-width-small uk-align-center">
            <img src="assets/icons/logo-dark.svg" class="logo-standard-height" alt="">
        </div><!-- ///uk-width-large -->

    </div><!-- //uk-container -->

</div><!-- //priamry-blue -->
<div class="uk-card uk-card-default uk-card-body uk-card-body-new uk-card-round bg-gray uk-card-with-icon"><span uk-icon="icon: user; ratio: 3;" class="uk-text-center color-white"></span></div>

<div class="bg-gray-light uk-padding-large">
    <div class="uk-container uk-container-small uk-margin-large-top">

        <ul uk-tab class="font-bolder font-medium" uk-tab="connect: #component-tab-contents; animation: uk-animation-fade">
            <li class="uk-active"><a style="padding-bottom: 20px;" class="font-gtWalsheimProMedium color-dark font-bolder" href="">New Account</a></li>
            <li><a style="padding-bottom: 20px;" href="" class="font-gtWalsheimProMedium color-dark font-bolder">Sign in</a></li>
        </ul>

        <ul id="component-tab-contents" class="uk-switcher">
            <li class="uk-active">
                <div class="max-wd-1600">
                    <div class="uk-section">
                        <div class="uk-child-width-1@m">
                            <form class="uk-form-horizontal uk-margin-large uk-padding-remove">

                                <div class="uk-margin flat-form-elem white-border-bottom font-gtWalsheimProRegular">
                                    <label class="uk-form-label font-medium font-bold color-dark" for="form-horizontal-text">Email Address</label>
                                    <div class="uk-form-controls">
                                        <input class="uk-input font-medium font-bold color-dark uk-text-right" id="form-horizontal-text" type="text" placeholder="some@email.com" value="John McGinn">
                                    </div>
                                </div>
                                <div class="uk-margin flat-form-elem white-border-bottom font-gtWalsheimProRegular">
                                    <label class="uk-form-label font-medium font-bold color-dark" for="form-horizontal-text">Password</label>
                                    <div class="uk-form-controls">
                                        <input class="uk-input font-medium font-bold color-dark uk-text-right" id="form-horizontal-text" type="password" value="password" placeholder="">
                                    </div>
                                </div>
                                <div class="uk-margin flat-form-elem white-border-bottom font-gtWalsheimProRegular">
                                    <label class="uk-form-label font-medium font-bold color-dark" for="form-horizontal-text">Full Name</label>
                                    <div class="uk-form-controls">
                                        <input class="uk-input font-medium font-bold color-dark uk-text-right" id="form-horizontal-text" type="text" value="Mt Pleasant, SC" placeholder="Some text...">
                                    </div>
                                </div>
                                <div class="uk-margin flat-form-elem white-border-bottom font-gtWalsheimProRegular">
                                    <label style="min-width: 50%;" class="uk-form-label font-medium font-bold color-dark" for="form-horizontal-select">I am an industry professional</label>
                                    <div class="uk-form-controls uk-clearfix uk-form-controls-text">
                                        <input id="form-horizontal-select" class="uk-checkbox color-dark-blue font-gtPressuraMonoLight font-smaller font-bold uk-align-right" type="checkbox" checked>
                                    </div>
                                </div>

                                <div class="uk-margin">
                                    <button class="uk-button uk-button-large uk-width-1-1 uk-margin-small-bottom font-medium font-bold color-dark bg-white font-gtPressuraMonoLight text-capitalize">Complete Account Setup</button>
                                </div>


                            </form>
                        </div>
                    </div><!-- //uk-section -->
                </div><!-- //max-wd-1600 -->
            </li>
            <li>
                <div class="max-wd-1600">
                    <div class="uk-section">
                        <div class="uk-child-width-1@m">
                            <form class="uk-form-horizontal uk-margin-large uk-padding-remove">

                                <div class="uk-margin flat-form-elem white-border-bottom font-gtWalsheimProRegular">
                                    <label class="uk-form-label font-medium font-bold color-dark" for="form-horizontal-text">Email Address</label>
                                    <div class="uk-form-controls">
                                        <input class="uk-input font-medium font-bold color-dark uk-text-right" id="form-horizontal-text" type="text" placeholder="some@email.com" value="John McGinn">
                                    </div>
                                </div>
                                <div class="uk-margin flat-form-elem white-border-bottom font-gtWalsheimProRegular">
                                    <label class="uk-form-label font-medium font-bold color-dark" for="form-horizontal-text">Password</label>
                                    <div class="uk-form-controls">
                                        <input class="uk-input font-medium font-bold color-dark uk-text-right" id="form-horizontal-text" type="password" value="password" placeholder="">
                                    </div>
                                </div>
                                <div class="uk-margin flat-form-elem white-border-bottom font-gtWalsheimProRegular">
                                    <label style="min-width: 50%;" class="uk-form-label font-medium font-bold color-dark" for="form-horizontal-select">Remember me?</label>
                                    <div class="uk-form-controls uk-clearfix uk-form-controls-text">
                                        <input id="form-horizontal-select" class="uk-checkbox color-dark-blue font-gtPressuraMonoLight font-smaller font-bold uk-align-right" type="checkbox" checked>
                                    </div>
                                </div>

                                <div class="uk-margin">
                                    <button class="uk-button uk-button-large uk-width-1-1 uk-margin-small-bottom font-medium font-bold color-dark bg-white font-gtPressuraMonoLight text-capitalize">Login</button>
                                </div>


                            </form>
                        </div>
                    </div><!-- //uk-section -->
                </div><!-- //max-wd-1600 -->
            </li>
        </ul>

    </div><!-- //uk-container -->
</div><!-- //bg-gray-light -->


<?php get_footer(); ?>
