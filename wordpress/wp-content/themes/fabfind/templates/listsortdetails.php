<?php /* Template Name : List Sort Details */ ?>

<?php get_header(); ?>

<div class="bg-gray">

    <div class="uk-container uk-container-small uk-padding">

        <div class="uk-width-small uk-align-left">
            <img src="assets/icons/logo-light.svg" class="logo-standard-height" alt="">
        </div><!-- ///uk-width-large -->
        <div class="uk-width-small uk-align-right font-gtWalsheimProRegular">
            <span class="uk-badge badge-medium border-blue bg-transparent font-bold color-white">3</span>&nbsp;
            <span class="uk-badge badge-medium border-dark-blue bg-transparent font-bold color-white">4</span>
        </div><!-- ///uk-width-large -->

    </div><!-- //uk-container -->

</div><!-- //bg-white -->

<div class="uk-section-muted">

    <div class="uk-container uk-container-small">
        <form uk-grid class="search-bar-form">
            <div class="uk-width-1-2@s uk-padding-small">
                <div class="uk-inline display-block">
                    <span class="uk-form-icon color-dark" uk-icon="icon: search"></span>
                    <input class="uk-input no-border font-gtPressuraMonoLight color-dark font-small font-bold" type="text" placeholder="Find your fab">
                </div>
            </div>
            <div class="uk-width-1-4@s uk-padding-small">
                <select class="uk-select no-border font-gtPressuraMonoLight color-dark font-small font-bold">
                    <option>Prefab Homes</option>
                </select>
            </div>
            <div class="uk-width-1-4@s uk-padding-remove-horizontal">
                <button class="uk-button uk-button-default no-border font-gtPressuraMonoLight text-capitalize color-dark font-small font-bold bg-blue  uk-padding-small search-btn">Search</button>
            </div>
        </form>
    </div><!-- //uk-container -->

    <br/><br/>

    <div class="uk-container uk-container-small">
        <div uk-grid>
          <h2 class="font-bolder color-dark font-gtWalsheimProRegular">Method Homes</h2>
        </div>
        <hr class="hr-short-new"/>
        <div uk-grid>
          <div class="uk-width-1-2@s">
            <p class="uk-text-meta font-gtWalsheimProRegular">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
          </div>
          <div class="uk-width-1-2@s">
            <ul class="uk-list uk-list-divider-white font-gtWalsheimProRegular font-bold color-dark">
              <li>Website <span class="uk-align-right color-orange">info.website.com</span></li>
              <li>Website <span class="uk-align-right">info.website.com</span></li>
              <li>Website <span class="uk-align-right">info.website.com</span></li>
              <li>Website <span class="uk-align-right">info.website.com</span></li>
            </ul>
          </div>
        </div>
    </div>
    <br/><br/>

    <div class="uk-container uk-container-small list-view-wrap">
        <h3 class="font-bolder color-dark font-gtWalsheimProRegular">Available Homes</h3>
        <div class="uk-grid-match uk-child-width-1-3@m" uk-grid>
            <div class="uk-width-1-4@m list-left">
                <div class="uk-background-secondary bg-light" style="height: 230px;"></div>
            </div>
            <div class="uk-width-expand@m uk-padding-small list-content bg-white">
                <h4 class="font-large font-bolder uk-pad no-margin font-gtWalsheimProRegular">Elemental Series<span class="uk-align-right color-orange font-bolder font-gtWalsheimProRegular"><small class="font-bold font-small"><span uk-icon="icon: bolt"></span> Method Name</small></span></h4>
                <p class="uk-margin-remove uk-padding-remove font-gtWalsheimProRegular">The Method Homes Elemental Series is a line of modern, efficient, and livable modular homes. Elemental features 6 different floorplans for you to customize. To streamline the design...</p>
                <p class="uk-text-center list-meta uk-margin-remove uk-padding-remove">
                    <span class="font-bolder font-gtPressuraMonoLight font-smaller color-dark">1200 - 2400 SQ FT</span>
                    <span class="font-bolder font-gtPressuraMonoLight font-smaller color-dark">3 BED</span>
                    <span class="font-bolder font-gtPressuraMonoLight font-smaller color-dark">2 BA</span>
                    <span class="font-bolder font-gtPressuraMonoLight font-smaller color-dark">$125k - $412k</span>
                    <span class="font-bolder font-gtPressuraMonoLight font-smaller color-dark">USA</span>
                </p>
            </div>
            <div class="uk-width-1-4@m list-right uk-padding-small left-border bg-white">
                <div class="font-bold font-gtPressuraMonoLight color-dark">Save<span class="uk-align-right"><a class="uk-icon-button color-dark font-bold" href="" uk-icon="icon: plus"></a></span></div>
                <div class="font-bold font-gtPressuraMonoLight color-dark">Compare<span class="uk-align-right"><a class="uk-icon-button color-dark font-bold" href="" uk-icon="icon: move"></a></span></div>
                <div class="font-bold font-gtPressuraMonoLight color-dark">View<span class="uk-align-right"><a class="uk-icon-button color-dark font-bold" href="" uk-icon="icon: chevron-right"></a></span></div>
            </div>
        </div>
        <br>
        <div class="uk-grid-match uk-child-width-1-3@m" uk-grid>
            <div class="uk-width-1-4@m list-left">
                <div class="uk-background-secondary bg-light" style="height: 230px;"></div>
            </div>
            <div class="uk-width-expand@m uk-padding-small list-content bg-white">
                <h4 class="font-large font-bolder uk-pad no-margin font-gtWalsheimProRegular">Elemental Series<span class="uk-align-right color-orange font-bolder font-gtWalsheimProRegular uk-margin-remove"><small class="color-dark-blue font-bold font-small"><span uk-icon="icon: bolt"></span> Featured Result</small></span></h4>
                <p class="uk-margin-remove uk-padding-remove font-gtWalsheimProRegular">The Method Homes Elemental Series is a line of modern, efficient, and livable modular homes. Elemental features 6 different floorplans for you to customize. To streamline the design...</p>
                <p class="uk-text-center list-meta uk-margin-remove uk-padding-remove">
                    <span class="font-bolder font-gtPressuraMonoLight font-smaller color-dark">1200 - 2400 SQ FT</span>
                    <span class="font-bolder font-gtPressuraMonoLight font-smaller color-dark">3 BED</span>
                    <span class="font-bolder font-gtPressuraMonoLight font-smaller color-dark">2 BA</span>
                    <span class="font-bolder font-gtPressuraMonoLight font-smaller color-dark">$125k - $412k</span>
                    <span class="font-bolder font-gtPressuraMonoLight font-smaller color-dark">USA</span>
                </p>
            </div>
            <div class="uk-width-1-4@m list-right uk-padding-small left-border bg-white">
                <div class="font-bold font-gtPressuraMonoLight color-dark">Save<span class="uk-align-right"><a class="uk-icon-button color-dark font-bold" href="" uk-icon="icon: plus"></a></span></div>
                <div class="font-bold font-gtPressuraMonoLight color-dark">Compare<span class="uk-align-right"><a class="uk-icon-button color-dark font-bold" href="" uk-icon="icon: move"></a></span></div>
                <div class="font-bold font-gtPressuraMonoLight color-dark">View<span class="uk-align-right"><a class="uk-icon-button color-dark font-bold" href="" uk-icon="icon: chevron-right"></a></span></div>
            </div>
        </div>
        <br>
        <div class="uk-grid-match uk-child-width-1-3@m" uk-grid>
            <div class="uk-width-1-4@m list-left">
                <div class="uk-background-secondary bg-light" style="height: 230px;"></div>
            </div>
            <div class="uk-width-expand@m uk-padding-small list-content bg-white">
                <h4 class="font-large font-bolder uk-pad no-margin font-gtWalsheimProRegular">Elemental Series<span class="uk-align-right color-orange font-bolder font-gtWalsheimProRegular uk-margin-remove"><small class="color-dark-blue font-bold font-small"><span uk-icon="icon: bolt"></span> Featured Result</small></span></h4>
                <p class="uk-margin-remove uk-padding-remove font-gtWalsheimProRegular">The Method Homes Elemental Series is a line of modern, efficient, and livable modular homes. Elemental features 6 different floorplans for you to customize. To streamline the design...</p>
                <p class="uk-text-center list-meta uk-margin-remove uk-padding-remove">
                    <span class="font-bolder font-gtPressuraMonoLight font-smaller color-dark">1200 - 2400 SQ FT</span>
                    <span class="font-bolder font-gtPressuraMonoLight font-smaller color-dark">3 BED</span>
                    <span class="font-bolder font-gtPressuraMonoLight font-smaller color-dark">2 BA</span>
                    <span class="font-bolder font-gtPressuraMonoLight font-smaller color-dark">$125k - $412k</span>
                    <span class="font-bolder font-gtPressuraMonoLight font-smaller color-dark">USA</span>
                </p>
            </div>
            <div class="uk-width-1-4@m list-right uk-padding-small left-border bg-white">
                <div class="font-bold font-gtPressuraMonoLight color-dark">Save<span class="uk-align-right"><a class="uk-icon-button color-dark font-bold" href="" uk-icon="icon: plus"></a></span></div>
                <div class="font-bold font-gtPressuraMonoLight color-dark">Compare<span class="uk-align-right"><a class="uk-icon-button color-dark font-bold" href="" uk-icon="icon: move"></a></span></div>
                <div class="font-bold font-gtPressuraMonoLight color-dark">View<span class="uk-align-right"><a class="uk-icon-button color-dark font-bold" href="" uk-icon="icon: chevron-right"></a></span></div>
            </div>
        </div>
        <br>

    </div><!-- //uk-container -->


</div><!-- //uk-section-muted -->

<?php get_footer(); ?>
