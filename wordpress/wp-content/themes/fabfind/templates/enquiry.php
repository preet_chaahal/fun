<?php /* Template Name : Enquiry */ ?>

<?php get_header(); ?>

<div class="bg-white">

    <div class="uk-container uk-container-small uk-padding">

        <div class="uk-width-small uk-align-center">
            <img src="assets/icons/logo-dark.svg" class="" alt="">
        </div><!-- ///uk-width-large -->

    </div><!-- //uk-container -->

</div><!-- //priamry-blue -->
<div class="uk-hidden uk-container uk-container-small">
    <div style="width: 350px;" class="uk-child-width-expand@s uk-grid-small" uk-grid>
        <div class="uk-width-small">
            <div class="uk-card uk-padding-remove uk-card-default uk-card-body uk-card-body-new uk-card-round uk-align-rigth">
            </div>
        </div>
    </div>
</div>

<div class="uk-section uk-padding-remove uk-margin-remove">
    <div class="uk-container uk-container-small uk-text-center" style="max-width: 340px;">

                <div class="uk-card uk-card-small uk-display-inline-block uk-padding-remove uk-card-default  uk-card-round uk-card-body uk-align-center uk-card-body-new uk-margin-right">
                </div>
                <div class="uk-card uk-card-small uk-padding-remove uk-display-inline-block uk-card-default  uk-card-round uk-align-center uk-card-body uk-card-body-new">
                </div>


    </div>
</div>


<div class="bg-gray-light uk-padding-large">
    <div class="uk-container uk-container-small">

        <div class="max-wd-1600">
            <div class="uk-section">

                <div class="uk-child-width-1@m" uk-grid>
                    <h2 class="color-dark font-bold font-gtWalsheimProMedium uk-text-center">Enquiry Form</h2>
                </div>
                <div class="uk-child-width-1@m">
                    <form class="uk-form-horizontal uk-margin-large uk-padding-large">

                        <div class="uk-margin flat-form-elem white-border-bottom font-gtWalsheimProRegular">
                            <label class="uk-form-label font-medium font-bold color-dark" for="form-horizontal-text">Email</label>
                            <div class="uk-form-controls">
                                <input class="uk-input font-medium font-bold color-dark uk-text-right" id="form-horizontal-text" type="text" placeholder="Some text..." value="John McGinn">
                            </div>
                        </div>
                        <div class="uk-margin flat-form-elem white-border-bottom font-gtWalsheimProRegular">
                            <label class="uk-form-label font-medium font-bold color-dark" for="form-horizontal-text">Telephone</label>
                            <div class="uk-form-controls">
                                <input class="uk-input font-medium font-bold color-dark uk-text-right" id="form-horizontal-text" type="text" placeholder="Some text..." value="John McGinn">
                            </div>
                        </div>
                        <div class="uk-margin flat-form-elem white-border-bottom font-gtWalsheimProRegular">
                            <label class="uk-form-label font-medium font-bold color-dark" for="form-horizontal-text">Full Name</label>
                            <div class="uk-form-controls">
                                <input class="uk-input font-medium font-bold color-dark uk-text-right" id="form-horizontal-text" type="text" placeholder="Some text..." value="John McGinn">
                            </div>
                        </div>
                        <div class="uk-margin flat-form-elem white-border-bottom font-gtWalsheimProRegular">
                            <label style="min-width: 50%;" class="uk-form-label font-medium font-bold color-dark" for="form-horizontal-select">I own land</label>
                            <div class="uk-form-controls uk-clearfix uk-form-controls-text">
                                <input id="form-horizontal-select" class="uk-checkbox color-dark-blue font-gtPressuraMonoLight font-smaller font-bold uk-align-right" type="checkbox" checked>
                            </div>
                        </div>
                        <div class="uk-margin flat-form-elem white-border-bottom font-gtWalsheimProRegular">
                            <label class="uk-form-label font-medium font-bold color-dark" for="form-horizontal-text">Project Location</label>
                            <div class="uk-form-controls">
                                <input class="uk-input font-medium font-bold color-dark uk-text-right" id="form-horizontal-text" type="text" value="Mt Pleasant, SC" placeholder="Some text...">
                            </div>
                        </div>
                        <div class="uk-margin flat-form-elem">
                            <label class="uk-form-label font-medium font-bold color-dark" for="form-horizontal-select">Project Kickoff</label>
                            <div class="uk-form-controls">
                                <select class="uk-select no-border font-medium font-bold color-dark uk-text-right">
                                    <option>2017</option>
                                    <option>2016</option>
                                </select>
                            </div>
                        </div>
                        <div class="uk-margin flat-form-elem">
                            <label class="uk-form-label font-medium font-bold color-dark" for="form-horizontal-select">Type of project</label>
                            <div class="uk-form-controls">
                                <select class="uk-select no-border font-medium font-bold color-dark uk-text-right">
                                    <option>Vacation</option>
                                    <option>Distributers</option>
                                </select>
                            </div>
                        </div>
                        <div class="uk-margin flat-form-elem">
                            <label class="uk-form-label font-medium font-bold color-dark" for="form-horizontal-select">Budget Range</label>
                            <div class="uk-form-controls">
                                <select class="uk-select no-border font-medium font-bold color-dark uk-text-right">
                                    <option>$100 , 000</option>
                                    <option>$200 , 000</option>
                                </select>
                            </div>
                        </div>
                        <div class="uk-margin flat-form-elem">
                            <label class="uk-form-label font-medium font-bold color-dark" for="form-horizontal-select">Square Footage</label>
                            <div class="uk-form-controls">
                                <select class="uk-select no-border font-medium font-bold color-dark uk-text-right">
                                    <option>1500 sqft</option>
                                    <option>2000 sqft</option>
                                </select>
                            </div>
                        </div>
                        <div class="uk-margin flat-form-elem white-border-bottom font-gtWalsheimProRegular">
                            <label class="uk-form-label font-medium font-bold color-dark" for="form-horizontal-text">Other Notes</label>
                            <div class="uk-form-controls">
                                <input class="uk-input font-medium font-bold color-dark uk-text-right" id="form-horizontal-text" type="text" placeholder="Some text...">
                            </div>
                        </div>
                        <div class="uk-margin flat-form-elem white-border-bottom font-gtWalsheimProRegular">
                            <label class="uk-form-label font-medium font-bold color-dark" for="form-horizontal-text">Please tell us more</label>
                            <div class="uk-form-controls">
                                <input class="uk-input font-medium font-bold color-dark uk-text-right" id="form-horizontal-text" type="text" placeholder="Some text...">
                            </div>
                        </div>
                        <div class="uk-margin">
                            <button class="uk-button uk-button-large uk-width-1-1 uk-margin-small-bottom bg-white font-medium font-bold color-dark font-gtPressuraMonoLight text-capitalize">Submit Enquiry</button>
                        </div>

                    </form>
                </div>

            </div><!-- //uk-section -->
        </div><!-- //max-wd-1600 -->

    </div><!-- //uk-container -->
</div><!-- //bg-gray-light -->

<?php get_footer(); ?>
