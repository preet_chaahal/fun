<?php /* Template Name: Comparisons Page */ ?>

<?php get_header(); ?>

<div class="bg-white">

    <div class="uk-container uk-container-small uk-padding">

        <div class="uk-width-small uk-align-center">
            <img src="assets/icons/logo-dark.svg" class="uk-responsive-height logo-standard-height" alt="">
        </div><!-- ///uk-width-large -->

    </div><!-- //uk-container -->

</div><!-- //primary-blue -->

<div class="uk-card uk-card-default uk-card-body uk-card-body-new uk-card-round">
</div>

<div class="bg-gray-light">

    <div class="max-wd-1600">
    <div class="uk-container uk-container-expand uk-padding-large">

            <div class="uk-child-width-1@s" style="height: 30px;"></div>
        <div class="uk-child-width-1@m uk-padding-small" uk-grid>

              <p class="uk-text-center badges-wrap">
                <span class="font-bolder font-gtWalsheimProRegular color-dark">Profile <span class="uk-badge badge-medium uk-hidden"></span></span>
                <span class="font-bolder font-gtWalsheimProRegular color-dark">Saved <span class="uk-badge badge-medium bg-blue color-dark">3</span></span>
              <span class="font-bolder font-gtWalsheimProRegular color-dark">Comparisons <span class="uk-badge badge-medium bg-dark-blue color-white">4</span></span>
            </p>
        </div>
        <div uk-grid class="uk-grid-small uk-flex uk-overflow-auto">

            <table class="uk-table uk-table-small uk-table-frappedev uk-table-middle uk-overflow-auto">
              <tbody>
                  <tr>
                    <th>
                      <h4 class="color-dark font-bolder font-gtWalsheimProRegular">Specifications</h4>
                    </th>
                    <th>
                      <div>
                        <div class="uk-card uk-card-default uk-card-no-shadow" style="position: relative;">
                            <div class="uk-card-media-top">
                                <img width="100%" src="assets/images/house-1.png" alt="">
                                <div style="border-radius: 100%; top: 20px; right: 20px; padding: 20px;" class="uk-position-top-right uk-overlay uk-overlay-default"></div>
                            </div>
                            <div class="uk-card-body uk-card-body-new">
                                <h4 class="color-dark font-bolder font-gtWalsheimProRegular"><center>Specifications</center></h4>
                            </div>
                        </div>
                      </div>
                    </th>
                    <th>
                      <div>
                        <div class="uk-card uk-card-default uk-card-no-shadow" style="position: relative;">
                            <div class="uk-card-media-top">
                                <img width="100%" src="assets/images/house-2.png" alt="">
                                <div style="border-radius: 100%; top: 20px; right: 20px; padding: 20px;" class="uk-position-top-right uk-overlay uk-overlay-default"></div>
                            </div>
                            <div class="uk-card-body uk-card-body-new">
                                <h4 class="color-dark font-bolder font-gtWalsheimProRegular"><center>Specifications</center></h4>
                            </div>
                        </div>
                      </div>
                    </th>
                    <th>
                      <div>
                        <div class="uk-card uk-card-default uk-card-no-shadow" style="position: relative;">
                            <div class="uk-card-media-top">
                                <img width="100%" src="assets/images/house-3.png" alt="">
                                <div style="border-radius: 100%; top: 20px; right: 20px; padding: 20px;" class="uk-position-top-right uk-overlay uk-overlay-default"></div>
                            </div>
                            <div class="uk-card-body uk-card-body-new">
                                <h4 class="color-dark font-bolder font-gtWalsheimProRegular"><center>Specifications</center></h4>
                            </div>
                        </div>
                      </div>
                    </th>
                    <th>
                      <div>
                        <div class="uk-card uk-card-default uk-card-no-shadow" style="position: relative;">
                            <div class="uk-card-media-top">
                                <img width="100%" src="assets/images/house-4.png" alt="">
                                <div style="border-radius: 100%; top: 20px; right: 20px; padding: 20px;" class="uk-position-top-right uk-overlay uk-overlay-default"></div>
                            </div>
                            <div class="uk-card-body uk-card-body-new">
                                <h4 class="color-dark font-bolder font-gtWalsheimProRegular"><center>Specifications</center></h4>
                            </div>
                        </div>
                      </div>
                    </th>
                  </tr>
                  <tr class="white-border-bottom">
                      <td>Table Data</td>
                      <td>Table Data</td>
                      <td>Table Data</td>
                      <td>Table Data</td>
                      <td>Table Data</td>
                  </tr>
                  <tr class="white-border-bottom">
                      <td>Table Data</td>
                      <td>Table Data</td>
                      <td>Table Data</td>
                      <td>Table Data</td>
                      <td>Table Data</td>
                  </tr>
                  <tr class="white-border-bottom">
                      <td>Table Data</td>
                      <td>Table Data</td>
                      <td>Table Data</td>
                      <td>Table Data</td>
                      <td>Table Data</td>
                  </tr>
                  <tr class="white-border-bottom">
                      <td>Table Data</td>
                      <td>Table Data</td>
                      <td>Table Data</td>
                      <td>Table Data</td>
                      <td>Table Data</td>
                  </tr>
                  <tr class="white-border-bottom">
                      <td>Table Data</td>
                      <td>Table Data</td>
                      <td>Table Data</td>
                      <td>Table Data</td>
                      <td>Table Data</td>
                  </tr>
                  <tr class="white-border-bottom">
                      <td>Table Data</td>
                      <td>Table Data</td>
                      <td>Table Data</td>
                      <td>Table Data</td>
                      <td>Table Data</td>
                  </tr>
                  <tr class="white-border-bottom">
                      <td>Table Data</td>
                      <td>Table Data</td>
                      <td>Table Data</td>
                      <td>Table Data</td>
                      <td>Table Data</td>
                  </tr>
                  <tr class="white-border-bottom">
                      <td>Table Data</td>
                      <td>Table Data</td>
                      <td>Table Data</td>
                      <td>Table Data</td>
                      <td>Table Data</td>
                  </tr>
                  <tr class="white-border-bottom">
                      <td>Table Data</td>
                      <td>Table Data</td>
                      <td>Table Data</td>
                      <td>Table Data</td>
                      <td>Table Data</td>
                  </tr>
                  <tr class="white-border-bottom">
                      <td>Table Data</td>
                      <td>Table Data</td>
                      <td>Table Data</td>
                      <td>Table Data</td>
                      <td>Table Data</td>
                  </tr>
                  <tr class="white-border-bottom">
                      <td>Table Data</td>
                      <td>Table Data</td>
                      <td>Table Data</td>
                      <td>Table Data</td>
                      <td>Table Data</td>
                  </tr>
                  <tr class="white-border-bottom">
                      <td>Table Data</td>
                      <td>Table Data</td>
                      <td>Table Data</td>
                      <td>Table Data</td>
                      <td>Table Data</td>
                  </tr>
              </tbody>
            </table>

            <table class="uk-table uk-table-small uk-table-frappedev uk-table-middle uk-overflow-auto">
              <tbody>
                  <tr>
                    <th colspan="5">
                      <h4 class="color-dark font-bolder font-gtWalsheimProRegular">Additional Features</h4>
                    </th>
                  </tr>
                  <tr class="white-border-bottom">
                      <td>Table Data</td>
                      <td>Table Data</td>
                      <td>Table Data</td>
                      <td>Table Data</td>
                      <td>Table Data</td>
                  </tr>
                  <tr class="white-border-bottom">
                      <td>Table Data</td>
                      <td>Table Data</td>
                      <td>Table Data</td>
                      <td>Table Data</td>
                      <td>Table Data</td>
                  </tr>
                  <tr class="white-border-bottom">
                      <td>Table Data</td>
                      <td>Table Data</td>
                      <td>Table Data</td>
                      <td>Table Data</td>
                      <td>Table Data</td>
                  </tr>
                  <tr class="white-border-bottom">
                      <td>Table Data</td>
                      <td>Table Data</td>
                      <td>Table Data</td>
                      <td>Table Data</td>
                      <td>Table Data</td>
                  </tr>
                  <tr class="white-border-bottom">
                      <td>Table Data</td>
                      <td>Table Data</td>
                      <td>Table Data</td>
                      <td>Table Data</td>
                      <td>Table Data</td>
                  </tr>
                  <tr class="white-border-bottom">
                      <td>Table Data</td>
                      <td>Table Data</td>
                      <td>Table Data</td>
                      <td>Table Data</td>
                      <td>Table Data</td>
                  </tr>
              </tbody>
            </table>

            <table class="uk-table uk-table-small uk-table-frappedev uk-table-middle uk-overflow-auto">
              <tbody>
                  <tr>
                    <th colspan="5">
                      <h4 class="color-dark font-bolder font-gtWalsheimProRegular">Costs</h4>
                    </th>
                  </tr>
                  <tr class="white-border-bottom">
                      <td>Table Data</td>
                      <td>Table Data</td>
                      <td>Table Data</td>
                      <td>Table Data</td>
                      <td>Table Data</td>
                  </tr>
                  <tr class="white-border-bottom">
                      <td>Table Data</td>
                      <td>Table Data</td>
                      <td>Table Data</td>
                      <td>Table Data</td>
                      <td>Table Data</td>
                  </tr>
                  <tr class="white-border-bottom">
                      <td>Table Data</td>
                      <td>Table Data</td>
                      <td>Table Data</td>
                      <td>Table Data</td>
                      <td>Table Data</td>
                  </tr>
                  <tr class="white-border-bottom">
                      <td>Table Data</td>
                      <td>Table Data</td>
                      <td>Table Data</td>
                      <td>Table Data</td>
                      <td>Table Data</td>
                  </tr>
                  <tr class="white-border-bottom">
                      <td>Table Data</td>
                      <td>Table Data</td>
                      <td>Table Data</td>
                      <td>Table Data</td>
                      <td>Table Data</td>
                  </tr>
                  <tr class="white-border-bottom">
                      <td>Table Data</td>
                      <td>Table Data</td>
                      <td>Table Data</td>
                      <td>Table Data</td>
                      <td>Table Data</td>
                  </tr>
              </tbody>
            </table>

            <table class="uk-table uk-table-small uk-table-frappedev uk-table-middle uk-overflow-auto">
              <tbody>
                  <tr>
                    <th colspan="5">
                      <h4 class="color-dark font-bolder font-gtWalsheimProRegular">Certifications</h4>
                    </th>
                  </tr>
                  <tr class="white-border-bottom">
                      <td>Table Data</td>
                      <td><a disabled="disabled" href="" class="uk-icon-button bg-gray-normal" uk-icon="icon: tw"></a></td>
                      <td>Table Data</td>
                      <td><a disabled="disabled" href="" class="uk-icon-button bg-gray-normal" uk-icon="icon: tw"></a></td>
                      <td>Table Data</td>
                  </tr>
                  <tr class="white-border-bottom">
                      <td>Table Data</td>
                      <td>Table Data</td>
                      <td>Table Data</td>
                      <td>Table Data</td>
                      <td>Table Data</td>
                  </tr>
                  <tr class="white-border-bottom">
                      <td>Table Data</td>
                      <td>Table Data</td>
                      <td>Table Data</td>
                      <td>Table Data</td>
                      <td>Table Data</td>
                  </tr>
                  <tr class="white-border-bottom">
                      <td>Table Data</td>
                      <td>Table Data</td>
                      <td>Table Data</td>
                      <td>Table Data</td>
                      <td>Table Data</td>
                  </tr>
                  <tr class="white-border-bottom">
                      <td>Table Data</td>
                      <td>Table Data</td>
                      <td>Table Data</td>
                      <td>Table Data</td>
                      <td>Table Data</td>
                  </tr>
                  <tr class="white-border-bottom">
                      <td>Table Data</td>
                      <td>Table Data</td>
                      <td>Table Data</td>
                      <td>Table Data</td>
                      <td>Table Data</td>
                  </tr>
              </tbody>
            </table>

        </div>


      </div><!-- //uk-container -->
</div>
</div>
<?php get_footer(); ?>
