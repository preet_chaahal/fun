<?php
/*
** Functions file for theme functions and definitions
*/

/*
** Defining constants
*/
define('THEMEROOT', get_stylesheet_directory_uri());
define('THEME_ASSETS', get_stylesheet_directory_uri() . "/assets");


/*
** Setting the content width
*/
if( ! isset( $content_width) ) {
    $content_width = 800;
}

/*
** setup up the theme
*/

if ( ! function_exists( 'fabfind_setup' ) ) {

    function fabfind_setup() {
        /*
        ** Make the theme available for translation
        */

        $lang_dir = THEMEROOT . '/languages';
        load_theme_textdomain( 'fabfind', $lang_dir);

        add_theme_support( 'post-formats', array(
            'gallery', 'image', 'link', 'quote', 'video', 'audio'
        ));

        /*
        ** Add support for post thumbails
        */
        add_theme_support( 'post-thumbnails' );

        $defaults = array(
        	'default-image'          => get_stylesheet_directory_uri() . "/assets/images/main-carousel.png",
            'width'                  => '2000',
            'height'                 => '1000',
        	'flex-height'            => true,
        	'flex-width'             => true,
        	'uploads'                => true,
        	'random-default'         => false,
        	'header-text'            => true,
        	'default-text-color'     => '',
        	'wp-head-callback'       => '',
        	'admin-head-callback'    => '',
        	'admin-preview-callback' => ''
        );
        add_theme_support( 'custom-header', $defaults);

    }


    /*
    ** Now coming to dynamic and admin functionalities of the theme
    */

    /**
    * Register a custom menu page.
    */
    function add_fabfind_to_menu(){
        add_menu_page(
            __( 'FabFind', 'textdomain' ),
            'FabFind Manager',
            'manage_options',
            'fabfind',
            'fabfind_admin_page',
            'dashicons-admin-home',
            6
        );
    }

    function add_submenu_to_fabfind () {
        add_submenu_page( 'fabfind', 'FabFind Featured Blog', 'Front Page Editor', 'manage_options', 'fabfind_front_page_menu', 'fabfind_front_page_editor');
    }

    add_action( 'admin_menu', 'add_fabfind_to_menu' );
    add_action( 'admin_menu', 'add_submenu_to_fabfind' );


    /**
    * Display a custom menu page
    */
    function fabfind_admin_page() {
    ?>
        <h1>Welcome to managing FabFind Theme</h1>
    <?php
    }

    function fabfind_front_page_editor () {
    ?>
    <?php
    }


    /*
    * Creating a function to create our CPT
    */

    function custom_post_type_homes() {

    // Set UI labels for Custom Post Type
    	$labels = array(
    		'name'                => _x( 'Homes', 'Post Type General Name', 'fabfind' ),
    		'singular_name'       => _x( 'Home', 'Post Type Singular Name', 'fabfind' ),
    		'menu_name'           => __( 'FabFind Homes', 'fabfind' ),
    		'parent_item_colon'   => __( 'Parent Home', 'fabfind' ),
    		'all_items'           => __( 'All Homes', 'fabfind' ),
    		'view_item'           => __( 'View Home', 'fabfind' ),
    		'add_new_item'        => __( 'Add New Home', 'fabfind' ),
    		'add_new'             => __( 'Add New', 'fabfind' ),
    		'edit_item'           => __( 'Edit Home', 'fabfind' ),
    		'update_item'         => __( 'Update Home', 'fabfind' ),
    		'search_items'        => __( 'Search Home', 'fabfind' ),
    		'not_found'           => __( 'Not Found', 'fabfind' ),
    		'not_found_in_trash'  => __( 'Not found in Trash', 'fabfind' ),
    	);

    // Set other options for Custom Post Type

    	$args = array(
    		'label'               => __( 'homes', 'fabfind' ),
    		'description'         => __( 'Home news and reviews', 'fabfind' ),
    		'labels'              => $labels,
    		// Features this CPT supports in Post Editor
    		'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields', 'categories'),
    		// You can associate this CPT with a taxonomy or custom taxonomy.
    		'taxonomies'          => array( 'genres' ),
    		/* A hierarchical CPT is like Pages and can have
    		* Parent and child items. A non-hierarchical CPT
    		* is like Posts.
    		*/
    		'hierarchical'        => false,
    		'public'              => true,
    		'show_ui'             => true,
    		'show_in_menu'        => true,
    		'show_in_nav_menus'   => true,
    		'show_in_admin_bar'   => true,
    		'menu_position'       => 5,
    		'can_export'          => true,
    		'has_archive'         => true,
            'has_categories'      => true,
            'categories'          => true,
    		'exclude_from_search' => false,
    		'publicly_queryable'  => true,
    		// 'capability_type'     => 'page',
            'menu_icon'           => 'dashicons-admin-home'
    	);

    	// Registering your Custom Post Type
    	register_post_type( 'homes', $args );

    }



    /* Hook into the 'init' action so that the function
    * Containing our post type registration is not
    * unnecessarily executed.
    */

    add_action( 'init', 'custom_post_type_homes', 0 );


    /*
    * Creating a function to create our CPT
    */

    function custom_post_type_manufacturers() {

    // Set UI labels for Custom Post Type
        $labels = array(
            'name'                => _x( 'Manufacturers', 'Post Type General Name', 'fabfind' ),
            'singular_name'       => _x( 'Manufacturer', 'Post Type Singular Name', 'fabfind' ),
            'menu_name'           => __( 'FabFind Manufacturers', 'fabfind' ),
            'parent_item_colon'   => __( 'Parent Manufacturer', 'fabfind' ),
            'all_items'           => __( 'All Manufacturers', 'fabfind' ),
            'view_item'           => __( 'View Manufacturer', 'fabfind' ),
            'add_new_item'        => __( 'Add New Manufacturer', 'fabfind' ),
            'add_new'             => __( 'Add New', 'fabfind' ),
            'edit_item'           => __( 'Edit Manufacturer', 'fabfind' ),
            'update_item'         => __( 'Update Manufacturer', 'fabfind' ),
            'search_items'        => __( 'Search Manufacturer', 'fabfind' ),
            'not_found'           => __( 'Not Found', 'fabfind' ),
            'not_found_in_trash'  => __( 'Not found in Trash', 'fabfind' ),
        );

    // Set other options for Custom Post Type

        $args = array(
            'label'               => __( 'manufacturers', 'fabfind' ),
            'description'         => __( 'Home news and reviews', 'fabfind' ),
            'labels'              => $labels,
            // Features this CPT supports in Post Editor
            'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields', 'categories'),
            // You can associate this CPT with a taxonomy or custom taxonomy.
            'taxonomies'          => array( 'genres' ),
            /* A hierarchical CPT is like Pages and can have
            * Parent and child items. A non-hierarchical CPT
            * is like Posts.
            */
            'hierarchical'        => false,
            'public'              => true,
            'show_ui'             => true,
            'show_in_menu'        => true,
            'show_in_nav_menus'   => true,
            'show_in_admin_bar'   => true,
            'menu_position'       => 5,
            'can_export'          => true,
            'has_archive'         => true,
            'has_categories'      => true,
            'categories'          => true,
            'exclude_from_search' => false,
            'publicly_queryable'  => true,
            // 'capability_type'     => 'page',
            'menu_icon'           => 'dashicons-admin-users'
        );

        // Registering your Custom Post Type
        register_post_type( 'manufacturers', $args );

    }

    /* Hook into the 'init' action so that the function
    * Containing our post type registration is not
    * unnecessarily executed.
    */

    add_action( 'init', 'custom_post_type_manufacturers', 0 );


    function fabfind_customize_register( $wp_customize ) {

        $wp_customize->add_section( 'site_tagline_editor' , array(
            'title'      => __( 'Site Front Page Tagline', 'fabfind' ),
            'priority'   => 30,
            ) );
        $wp_customize->add_setting( 'site_tagline' , array(
            'default'   => 'The fastest way to compare<br>and buy prefab homes.',
            'transport' => 'refresh',
        ) );
        $wp_customize->add_control(
         'site_tagline_control', //Set a unique ID for the control
         array(
            'label'      => __( 'Site Front Page Tagline', 'mytheme' ), //Admin-visible name of the control
            'settings'   => 'site_tagline', //Which setting to load and manipulate (serialized is okay)
            'priority'   => 10, //Determines the order this control appears in for the specified section
            'section'    => 'site_tagline_editor', //ID of the section this control should render in (can be one of yours, or a WordPress default section)
            'type' => 'textarea'
         ) );
    }
    add_action( 'customize_register', 'fabfind_customize_register' );

    add_action( 'after_setup_theme', 'fabfind_setup' );

    add_action( 'admin_init', 'add_meta_boxes' );

    function add_meta_boxes() {
        add_meta_box( 'some_metabox', 'Movies Relationship', 'movies_field', 'series' );
    }

    function movies_field() {
        global $post;
        $selected_movies = get_post_meta( $post->ID, '_movies', true );
        $all_movies = get_posts( array(
            'post_type' => 'movies',
            'numberposts' => -1,
            'orderby' => 'post_title',
            'order' => 'ASC'
        ) );
        ?>
        <input type="hidden" name="movies_nonce" value="<?php echo wp_create_nonce( basename( __FILE__ ) ); ?>" />
        <table class="form-table">
        <tr valign="top"><th scope="row">
        <label for="movies">Movies</label></th>
        <td><select multiple name="movies">
        <?php foreach ( $all_movies as $movie ) : ?>
            <option value="<?php echo $movie->ID; ?>"<?php echo (in_array( $movie->ID, $selected_movies )) ? ' selected="selected"' : ''; ?>><?php echo $movie->post_title; ?></option>
        <?php endforeach; ?>
        </select></td></tr>
        </table>

        <?php
    }

    add_action( 'save_post', 'save_movie_field' );

    function save_movie_field( $post_id ) {

        // only run this for series
        if ( 'series' != get_post_type( $post_id ) )
            return $post_id;

        // verify nonce
        if ( empty( $_POST['movies_nonce'] ) || !wp_verify_nonce( $_POST['movies_nonce'], basename( __FILE__ ) ) )
            return $post_id;

        // check autosave
        if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
            return $post_id;

        // check permissions
        if ( !current_user_can( 'edit_post', $post_id ) )
            return $post_id;

        // save
        update_post_meta( $post_id, '_movies', array_map( 'intval', $_POST['movies'] ) );

    }
}

?>
