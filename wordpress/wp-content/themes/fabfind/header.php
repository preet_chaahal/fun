<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage FabFind
 * @since 1.0
 */
?><!DOCTYPE html>
<html>
    <head>
        <title>FabFind</title>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="author" content="FrappeDev@preetChaahal" />
		<meta charset="<?php bloginfo( 'charset' ); ?>">
    	<meta name="viewport" content="width=device-width">
    	<link rel="profile" href="http://gmpg.org/xfn/11">
    	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

		<!-- stylesheets and scripts -->
		<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/css/uikit.min.css" />
        <script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/jquery-v2.2.4.min.js"></script>
        <script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/uikit.min.js"></script>
        <script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/uikit-icons.min.js"></script>
  		<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/css/main-style.css">
  		<!-- //End of stylesheets and scripts -->

		<style type="text/css">
		.wrapper {
			margin: 0 auto;
		}
		</style>

        <?php wp_head(); ?>
	</head>
	<body>

		<div class="wrapper">
